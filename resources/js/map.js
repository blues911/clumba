var csrf = $("meta[name=csrf_token]").attr('content');
// Тип поиска: radius, polygon. От этого параметра зависит результат на карте.
var searchType = $('#map').data('search-type');

// Модуль рисования на карте
require('./ext/paint_on_map.js');

ymaps.ready(initMap);

function initMap() {
    var currentZoom = 0;
    var $imagesGrid;
    var activeObjectId = null;
    var geoObjects = [];
    var map = new ymaps.Map("map", {
        center: [55.7558, 37.6173], // latitude, longitude
        controls: ['zoomControl', 'typeSelector'],
        zoom: 10
    });
    var objectManager = new ymaps.ObjectManager({
        clusterize: false,
        clusterIconLayout: ymaps.templateLayoutFactory.createClass('<div class="cluster-icon">{{ properties.geoObjects.length }}</div>'),
        clusterIconShape: {
            type: 'Rectangle',
            coordinates: [[-25, -25], [25, 25]]
        },
        clusterHideIconOnBalloonOpen: false,
        geoObjectHideIconOnBalloonOpen: false,
        clusterBalloonContentLayout: ymaps.templateLayoutFactory.createClass([
            '<ul class="cluster-placemarks-list">',
                '{% for geoObject in properties.geoObjects %}',
                    '<li data-id="{{ geoObject.data.id }}">{{ geoObject.data.address }} ({{geoObject.data.size}}, {{geoObject.data.media_type_title}})</li>',
                '{% endfor %}',
            '</ul>'
        ].join(''))
    });

    map.behaviors.disable('scrollZoom');

    map.events.add('boundschange', function (event) {
        currentZoom = event.get('newZoom');
        // console.log(currentZoom);

        if (event.get('newZoom') !== event.get('oldZoom')) {

            $('#placemark-info').removeClass('active');
            setPlacemarkInfo({});
            resetPlacemarkIcon();

            if (currentZoom <= 8) {
                objectManager.options.set({clusterize: true});
            } else {
                objectManager.options.set({clusterize: false});
                // !Есть проблема с появлением радиуса после отключения кластера,
                // приходится делать инициализацию точек повторно.
                if (event.get('oldZoom') == 8 && currentZoom == 9
                    || event.get('oldZoom') < 9 && currentZoom >= 9) {
                    initPlacemarks(false);
                }
            }

            if (currentZoom <= 10) {
                objectManager.objects.each(function (object) {
                    if (object.data != null) {
                        var icon = (object.data.building_type.map_icon != null) ? object.data.building_type.map_icon : 'default.png';
                        if (object.data.title.toLowerCase().includes('world class')) {
                            icon = 'world_class.png';
                        }
                        if (icon.match(/^burger_king|world_class|business_center|shopping_center|fitness_center(.*$)/)) {
                            icon = icon.replace(/.png/, '_sm.png');
                        }
                        objectManager.objects.setObjectOptions(object.id, {
                            iconImageHref: '/img/map/icons/' + icon,
                            iconImageSize: [10, 10],
                            iconImageOffset: [-5, -5]
                        });
                    }
                });
            } else if (currentZoom >= 11 && currentZoom <= 12) {
                objectManager.objects.each(function (object) {
                    if (object.data != null) {
                        var icon = (object.data.building_type.map_icon != null) ? object.data.building_type.map_icon : 'default.png';
                        if (object.data.title.toLowerCase().includes('world class')) {
                            icon = 'world_class.png';
                        }
                        objectManager.objects.setObjectOptions(object.id, {
                            iconImageHref: '/img/map/icons/' + icon,
                            iconImageSize: [32, 32],
                            iconImageOffset: [-16, -16]
                        });
                    }
                });
            } else if (currentZoom >= 13) {
                objectManager.objects.each(function (object) {
                    if (object.data != null) {
                        var icon = (object.data.building_type.map_icon != null) ? object.data.building_type.map_icon : 'default.png';
                        if (object.data.title.toLowerCase().includes('world class')) {
                            icon = 'world_class.png';
                        }
                        objectManager.objects.setObjectOptions(object.id, {
                            iconImageHref: '/img/map/icons/' + icon,
                            iconImageSize: [52, 52],
                            iconImageOffset: [-26, -26]
                        });
                    }
                });
            }
        }
    });

    $(document).ready(function() {
        // Кастомный скролл
        $('.placemark-info-params-images').niceScroll({
            cursorcolor: '#888888',
            cursorborder: '1px solid #888888',
            background: '#e6e6e6',
            cursoropacitymin: 1,
            cursorwidth: '6px',
            cursorborderradius: '1px',
            horizrailenabled: false,
            autohidemode: 'leave'
        });

        // Fix высоты карты относительно высоты экрана
        if ($(window).height() >= 700) {
            var mapHeight = ($(window).height() - $('#content').height() - 30) + $('#map').height();
            $('#map').css({'height': mapHeight + 'px'});
            map.container.fitToViewport();
        }

        initPlacemarks(true);
    });

    // Фильтры
    $('.custom-checkbox input[type="checkbox"]').on('change', function() {
        initPlacemarks(true);
    });

    $(document).on('click', 'ul.cluster-placemarks-list li', function(){
        var id = $(this).data('id');
        var items = $('#map').data('content');

        map.balloon.close();

        for (var i = 0; i < items.length; i++) {
            if (items[i]['id'] == id) {
                setPlacemarkInfo(items[i]);
                break;
            }
        }
    });

    // Окно информации по точке
    $('#placemark-info .close').on('click', function() {
        $('#placemark-info').removeClass('active');
        setPlacemarkInfo({});
        resetPlacemarkIcon();
    });

    // Окно изображений по точке
    $(document).on('click', '#placemark-info .images-grid-item img', function(e) {
        e.preventDefault();
        var id = Number($(this).attr('data-id'));
        var data = JSON.parse($('#placemark-info').attr('data-content'));
        setPlacemarkImage(data, id);
        $('#placemark-images').addClass('active');
    });

    $('#placemark-images .back').on('click', function() {
        $('#placemark-images').removeClass('active');
        $('#placemark-images .image-preview').css({'background-image': ''});
        $('#placemark-images img.prev').attr('data-id', 0);
        $('#placemark-images img.next').attr('data-id', 0);
        $('#placemark-images span.total').text('');
    });

    $('#placemark-images .close').on('click', function() {
        $('#placemark-info').removeClass('active');
        setPlacemarkInfo({});
        resetPlacemarkIcon();
        $('#placemark-images').removeClass('active');
        $('#placemark-images .image-preview').css({'background-image': ''});
        $('#placemark-images img.prev').attr('data-id', 0);
        $('#placemark-images img.next').attr('data-id', 0);
        $('#placemark-images span.total').text('');
    });

    $(document).on('click', '#placemark-images img.prev', function() {
        var id = Number($(this).attr('data-id'));
        var data = JSON.parse($('#placemark-info').attr('data-content'));
        setPlacemarkImage(data, id);
    });

    $(document).on('click', '#placemark-images img.next', function() {
        var id = Number($(this).attr('data-id'));
        var data = JSON.parse($('#placemark-info').attr('data-content'));
        setPlacemarkImage(data, id);
    });

    function initPlacemarks(zoomRange) {
        var uid = $('#map').data('uid');
        var buildingTypeIds = [];
        var mediaTypeIds = [];

        $('.building-type-checkbox:checked').each(function(i) {
            buildingTypeIds[i] = $(this).val();
        });

        $('.media-type-checkbox:checked').each(function(i) {
            mediaTypeIds[i] = $(this).val();
        });

        $.ajax({
            url: '/shared/ajax-calc-map-result',
            type: 'POST',
            data: {
                _token: csrf,
                uid: uid,
                building_type_ids: buildingTypeIds,
                media_type_ids: mediaTypeIds
            },
            dataType: 'JSON',
            success: function (result) {

                $('#map').attr('data-content', JSON.stringify(result));
                $('#placemark-info').removeClass('active');
                $('#placemark-images').removeClass('active');
                setPlacemarkInfo({});

                map.geoObjects.removeAll();
                objectManager.removeAll();
                geoObjects = [];

                if (result.length == 0) {
                    return false;
                }

                (searchType == 'radius') ? initByRadius(result, zoomRange) : initByPolygon(result, zoomRange);
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    function initByRadius(result, zoomRange) {
        for (var i = 0; i < result.length; i++) {
            // Установка круга
            geoObjects.push({
                type: 'Feature',
                id: 'circle_' + i,
                data: null,
                geometry: {
                    type: 'Circle',
                    coordinates: result[i]['address']['coords'],
                    radius: result[i]['radius']
                },
                options: {
                    fill: false,
                    fillColor: "#8593f977",
                    strokeColor: "#8593f9",
                    strokeWidth: 0
                }
            });

            // Установка центральной точки
            geoObjects.push({
                type: 'Feature',
                id: 'circle_center_' + i,
                data: null,
                geometry: {
                    type: 'Point',
                    coordinates: result[i]['address']['coords']
                },
                properties: {
                    hintContent: result[i]['address']['name'] + ' (радиус — ' + result[i]['radius'] + 'м)'
                },
                options: {
                    iconLayout: 'default#image',
                    iconImageHref: '/img/map/icons/default.png',
                    iconImageSize: [10, 10],
                    iconImageOffset: [-5, -5]
                }
            });

            // Установка меток находящихся в радиусе круга
            if (result[i]['points'].length > 0) {
                for (var j = 0; j < result[i]['points'].length; j++) {

                    var iconSettings = {
                        iconLayout: 'default#image',
                        iconImageHref: '',
                        iconImageSize: [],
                        iconImageOffset: []
                    };

                    var icon = (result[i]['points'][j]['building_type']['map_icon'] != null) ? result[i]['points'][j]['building_type']['map_icon'] : 'default.png';

                    if (result[i]['points'][j]['title'].toLowerCase().includes('world class')) {
                        icon = 'world_class.png';
                    }

                    if (currentZoom <= 10) {
                        if (icon.match(/^burger_king|world_class|business_center|shopping_center|fitness_center(.*$)/)) {
                            icon = icon.replace(/.png/, '_sm.png');
                        }
                        iconSettings.iconImageSize = [10, 10];
                        iconSettings.iconImageOffset = [-5, -5];
                    } else if (currentZoom >= 11 && currentZoom <= 12) {
                        iconSettings.iconImageSize = [32, 32];
                        iconSettings.iconImageOffset = [-16, -16];
                    } else if (currentZoom >= 13) {
                        iconSettings.iconImageSize = [52, 52];
                        iconSettings.iconImageOffset = [-26, -26];
                    }

                    iconSettings.iconImageHref = '/img/map/icons/' + icon;

                    geoObjects.push({
                        type: 'Feature',
                        id: result[i]['points'][j]['id'],
                        data: result[i]['points'][j],
                        geometry: {
                            type: 'Point',
                            coordinates:[result[i]['points'][j]['map_lat'], result[i]['points'][j]['map_lon']]
                        },
                        options: iconSettings
                    });
                }
            }
        }

        objectManager.add(geoObjects);

        objectManager.objects.events.add('click', function (e) {
            var objectId = e.get('objectId');
            var obj = objectManager.objects.getById(objectId);

            if (activeObjectId == objectId || obj.data == null) {
                return false;
            }
    
            if (activeObjectId != null && activeObjectId != objectId) {
                resetPlacemarkIcon();
            }

            activeObjectId = objectId;

            if (currentZoom <= 10) {
                objectManager.objects.setObjectOptions(objectId, {
                    iconImageHref: '/img/map/icons/selected.png',
                    iconImageSize: [10, 12],
                    iconImageOffset: [-5, -6]
                });
            } else if (currentZoom >= 11 && currentZoom <= 12) {
                objectManager.objects.setObjectOptions(objectId, {
                    iconImageHref: '/img/map/icons/selected.png',
                    iconImageSize: [24, 30],
                    iconImageOffset: [-12, -15]
                });
            } else if (currentZoom >= 13) {
                objectManager.objects.setObjectOptions(objectId, {
                    iconImageHref: '/img/map/icons/selected.png',
                    iconImageSize: [40, 50],
                    iconImageOffset: [-20, -25]
                });
            }

            map.setCenter(obj.geometry.coordinates);
            setPlacemarkInfo(obj.data);

            $('.region-list').removeClass('active');

        }).add('mouseenter', function (e) {

            var objectId = e.get('objectId');
            var obj = objectManager.objects.getById(objectId);

            if (obj.data == null && String(objectId).indexOf('circle_center_') == 0) {
                var circleId = String(objectId).replace('circle_center_', 'circle_');
                objectManager.objects.setObjectOptions(circleId, {
                      fill: true,
                      strokeWidth: 1
                });
            }

        }).add('mouseleave', function (e) {

            var objectId = e.get('objectId');
            var obj = objectManager.objects.getById(objectId);

            if (obj.data == null && String(objectId).indexOf('circle_center_') == 0) {
                var circleId = String(objectId).replace('circle_center_', 'circle_');
                objectManager.objects.setObjectOptions(circleId, {
                      fill: false,
                      strokeWidth: 0
                });
            }

        });

        map.geoObjects.add(objectManager);

        if (zoomRange == true) {
            map.setBounds(map.geoObjects.getBounds(), {
                checkZoomRange: true,
            });
        }
    }

    function initByPolygon(result, zoomRange) {
        for (var i = 0; i < result.length; i++) {
            // Установка полигона
            geoObjects.push({
                type: 'Feature',
                id: 'polygon_' + i,
                data: null,
                geometry: {
                    type: 'Polygon',
                    coordinates: [result[i]['polygon_coords']]
                },
                options: {
                    fillColor: "#8593f977",
                    strokeColor: "#8593f9",
                    strokeWidth: 2
                }
            });
            // Установка адресов (опционально)
            // В отличии от круга, адреса в полигоне необязательны
            if (i == 0 && result[i]['addresses'].length > 0) {
                for (var j = 0; j < result[i]['addresses'].length; j++) {
                    geoObjects.push({
                        type: 'Feature',
                        id: 'circle_center_' + j,
                        data: null,
                        geometry: {
                            type: 'Point',
                            coordinates: result[i]['addresses'][j]['coords']
                        },
                        properties: {
                            hintContent: result[i]['addresses'][j]['name']
                        },
                        options: {
                            iconLayout: 'default#image',
                            iconImageHref: '/img/map/icons/default.png',
                            iconImageSize: [10, 10],
                            iconImageOffset: [-5, -5]
                        }
                    });
                }
            }
            // Установка меток находящихся в полигоне
            if (result[i]['points'].length > 0) {
                for (var k = 0; k < result[i]['points'].length; k++) {
                    var iconSettings = {
                        iconLayout: 'default#image',
                        iconImageHref: '',
                        iconImageSize: [],
                        iconImageOffset: []
                    };

                    var icon = (result[i]['points'][k]['building_type']['map_icon'] != null) ? result[i]['points'][k]['building_type']['map_icon'] : 'default.png';

                    if (result[i]['points'][k]['title'].toLowerCase().includes('world class')) {
                        icon = 'world_class.png';
                    }

                    if (currentZoom <= 10) {
                        if (icon.match(/^burger_king|world_class|business_center|shopping_center|fitness_center(.*$)/)) {
                            icon = icon.replace(/.png/, '_sm.png');
                        }
                        iconSettings.iconImageSize = [10, 10];
                        iconSettings.iconImageOffset = [-5, -5];
                    } else if (currentZoom >= 11 && currentZoom <= 12) {
                        iconSettings.iconImageSize = [32, 32];
                        iconSettings.iconImageOffset = [-16, -16];
                    } else if (currentZoom >= 13) {
                        iconSettings.iconImageSize = [52, 52];
                        iconSettings.iconImageOffset = [-26, -26];
                    }

                    iconSettings.iconImageHref = '/img/map/icons/' + icon;

                    geoObjects.push({
                        type: 'Feature',
                        id: result[i]['points'][k]['id'],
                        data: result[i]['points'][k],
                        geometry: {
                            type: 'Point',
                            coordinates:[result[i]['points'][k]['map_lat'], result[i]['points'][k]['map_lon']]
                        },
                        options: iconSettings
                    });
                }
            }
        }

        objectManager.add(geoObjects);

        objectManager.objects.events.add('click', function (e) {
            var objectId = e.get('objectId');
            var obj = objectManager.objects.getById(objectId);

            if (activeObjectId == objectId || obj.data == null) {
                return false;
            }
    
            if (activeObjectId != null && activeObjectId != objectId) {
                resetPlacemarkIcon();
            }

            activeObjectId = objectId;

            if (currentZoom <= 10) {
                objectManager.objects.setObjectOptions(objectId, {
                    iconImageHref: '/img/map/icons/selected.png',
                    iconImageSize: [10, 12],
                    iconImageOffset: [-5, -6]
                });
            } else if (currentZoom >= 11 && currentZoom <= 12) {
                objectManager.objects.setObjectOptions(objectId, {
                    iconImageHref: '/img/map/icons/selected.png',
                    iconImageSize: [24, 30],
                    iconImageOffset: [-12, -15]
                });
            } else if (currentZoom >= 13) {
                objectManager.objects.setObjectOptions(objectId, {
                    iconImageHref: '/img/map/icons/selected.png',
                    iconImageSize: [40, 50],
                    iconImageOffset: [-20, -25]
                });
            }

            map.setCenter(obj.geometry.coordinates);
            setPlacemarkInfo(obj.data);

            $('.region-list').removeClass('active');

        });

        map.geoObjects.add(objectManager);

        if (zoomRange == true) {
            map.setBounds(map.geoObjects.getBounds(), {
                checkZoomRange: true,
            });
        }
    }

    function setPlacemarkInfo(obj) {
        if (typeof $imagesGrid == 'object') {
            $imagesGrid.masonry('destroy');
            $imagesGrid = undefined;
        }

        $('#placemark-info').removeClass('sm');

        if (Object.keys(obj).length === 0) {

            $('#placemark-info').attr('data-content', '');
            $('#placemark-info .placemark-info-title').text('');
            $('#placemark-info .placemark-info-address').text('');
            $('#placemark-info .placemark-info-params-media-type span.text').text('');
            $('#placemark-info .placemark-info-params-media-type').removeClass('hidden');
            $('#placemark-info .placemark-info-params-size span.text').text('');
            $('#placemark-info .placemark-info-params-size span.text').attr('title', '');
            $('#placemark-info .placemark-info-params-size').removeClass('hidden');
            $('#placemark-info .placemark-info-params-orientation span.text').text('');
            $('#placemark-info .placemark-info-params-orientation').removeClass('hidden');
            $('#placemark-info .placemark-info-params-images .images-grid').html('');
        } else {

            $('#placemark-info').attr('data-content', JSON.stringify(obj));
            $('#placemark-info').addClass('active');
            $('#placemark-info .placemark-info-title').text(obj.title);
            $('#placemark-info .placemark-info-address').text(obj.address);

            if (obj.media_type.title) {
                $('#placemark-info .placemark-info-params-media-type span.text').text(obj.media_type_orig_title);
                $('#placemark-info .placemark-info-params-media-type').removeClass('hidden');
            } else {
                $('#placemark-info .placemark-info-params-media-type span.text').text('');
                $('#placemark-info .placemark-info-params-media-type').addClass('hidden');
            }

            if (obj.size) {
                $('#placemark-info .placemark-info-params-size span.text').attr('title', obj.size);
                $('#placemark-info .placemark-info-params-size span.text').text(obj.size);
                $('#placemark-info .placemark-info-params-size').removeClass('hidden');
            } else {
                $('#placemark-info .placemark-info-params-size span.text').attr('title', '');
                $('#placemark-info .placemark-info-params-size span.text').text('');
                $('#placemark-info .placemark-info-params-size').addClass('hidden');
            }

            if (obj.orientation) {
                $('#placemark-info .placemark-info-params-orientation span.text').text(obj.orientation);
                $('#placemark-info .placemark-info-params-orientation').removeClass('hidden');
            } else {
                $('#placemark-info .placemark-info-params-orientation span.text').text('');
                $('#placemark-info .placemark-info-params-orientation').addClass('hidden');
            }

            $('#placemark-info .placemark-info-params-images .images-grid').html('');
            $(document).find('.placemark-info-params-images').getNiceScroll().resize();

            if (obj.files.length > 0) {
                if (obj.files.length == 1) {
                    $('#placemark-info .placemark-info-params-images .images-grid').append([
                        '<div class="images-grid-item single">',
                            '<img src="/images/'+obj.files[0].name+'?w=400" data-id="'+obj.files[0].id+'">',
                        '</div>',
                    ].join(''));
                } else {
                    for (var i = 0; i < obj.files.length; i++) {
                        $('#placemark-info .placemark-info-params-images .images-grid').append([
                            '<div class="images-grid-item">',
                                '<img src="/images/'+obj.files[i].name+'?w=400" data-id="'+obj.files[i].id+'">',
                            '</div>',
                        ].join(''));
                    }

                    $imagesGrid = $('.images-grid').masonry({
                        itemSelector: '.images-grid-item',
                        columnWidth: 195,
                        gutter: 10
                    });

                    $imagesGrid.imagesLoaded().progress( function() {
                        $imagesGrid.masonry('layout');
                    });
                }
            } else {
                $('#placemark-info').addClass('sm');
            }

            setTimeout(function(){
                $(document).find('.placemark-info-params-images').getNiceScroll().resize();
                $(document).find('.nicescroll-rails').css({'width': '6px'});
            }, 500);
        }
    }

    function setPlacemarkImage(data, id) {
        $('#placemark-images img.prev').removeClass('hidden');
        $('#placemark-images img.next').removeClass('hidden');
        $('#placemark-images span.total').removeClass('hidden');

        if (id != 0) {

            for (var i = 0; i < data.files.length; i++) {
                if (data.files[i]['id'] == id) {
                    var indx = i;
                    var prevId = (data.files[indx-1] != undefined) ? data.files[indx-1]['id'] : 0;
                    var nextId = (data.files[indx+1] != undefined) ? data.files[indx+1]['id'] : 0;
                    $('#placemark-images .image-preview').css({'background-image': 'url(/uploads/' + data.files[i]['name'] + ')'});
                    $('#placemark-images img.prev').attr('data-id', prevId);
                    $('#placemark-images img.next').attr('data-id', nextId);
                    $('#placemark-images span.total').text((indx+1)+'/'+data.files.length);
                    break;
                }
            }

            if (data.files.length == 1) {
                $('#placemark-images img.prev').addClass('hidden');
                $('#placemark-images img.next').addClass('hidden');
                $('#placemark-images span.total').addClass('hidden');
            }
        }
    }

    function resetPlacemarkIcon() {
        if (activeObjectId != null) {
            var iconSettings = {
                iconLayout: 'default#image',
                iconImageHref: '',
                iconImageSize: [],
                iconImageOffset: []
            };
            var oldObj = objectManager.objects.getById(activeObjectId);
            var icon = (oldObj.data.building_type.map_icon != null) ? oldObj.data.building_type.map_icon : 'default.png';

            if (oldObj.data.title.toLowerCase().includes('world class')) {
                icon = 'world_class.png';
            }

            if (currentZoom <= 10) {
                if (icon.match(/^burger_king|world_class|business_center|shopping_center|fitness_center(.*$)/)) {
                    icon = icon.replace(/.png/, '_sm.png');
                }
                iconSettings.iconImageSize = [10, 10];
                iconSettings.iconImageOffset = [-5, -5];
            } else if (currentZoom >= 11 && currentZoom <= 12) {
                iconSettings.iconImageSize = [32, 32];
                iconSettings.iconImageOffset = [-16, -16];
            } else if (currentZoom >= 13) {
                iconSettings.iconImageSize = [52, 52];
                iconSettings.iconImageOffset = [-26, -26];
            }

            iconSettings.iconImageHref = '/img/map/icons/' + icon;

            objectManager.objects.setObjectOptions(activeObjectId, iconSettings);
            activeObjectId = null;
        }
    }
}
// Список (таблица)

if ($('#region-list-block').length == 1) {

    $('.js-region-delete').on('click', function(e) {
        e.preventDefault();
        var self = $(this);
        bootbox.confirm({
            message: "Удалить регион?",
            buttons: {
                confirm: {
                    label: 'Да',
                    className: 'btn-sm btn-brand'
                },
                cancel: {
                    label: 'Нет',
                    className: 'btn-sm btn-light'
                }
            },
            callback: function (result) {
                if (result == true) {
                    self.next('form').submit();
                } else {
                    return;
                }
            }
        });
    });
}
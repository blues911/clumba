// Обработка меню
$(document).on('click', '.menu > li > a', function() {
    var $a = $(this);
    var $li = $a.parent();
    var $ul = $a.next();
    if ($li.hasClass('menu-open')) {
        $li.removeClass('menu-open');
        $a.find('i').attr('class', 'fa fa-plus');
        $ul.slideUp(500);
    } else {
        $li.addClass('menu-open');
        $ul.slideDown(500);
        $li.siblings().removeClass('menu-open');
        $li.siblings().find('ul').slideUp(500);
        $a.find('i').attr('class', 'fa fa-minus');
    }
});

// Выход из ЛК
$('#logout').on('click', function(e) {
    e.preventDefault();
    $(this).next('form').submit();
});
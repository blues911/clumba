// Список (таблица)

if ($('#ad-medium-list-block').length == 1) {

    $('.table-filters a.search, .table-filters a.search > i.fa').on('click', function(e){
        e.preventDefault();

        var extId = $('input[name="ext_id"]').val();
        var regionId = $('select[name="region_id"]').val();
        var buildingTypeId = $('select[name="building_type_id"]').val();
        var mediaTypeId = $('select[name="media_type_id"]').val();
        var params = '?ext_id='+ extId + '&region_id='+ regionId + '&building_type_id=' + buildingTypeId + '&media_type_id=' + mediaTypeId + '&page=1';

        window.location.href = window.location.origin + window.location.pathname + params;
    });

    $('.table-filters a.reset, .table-filters a.reset > i.fa').on('click', function(e){
        e.preventDefault();

        var params = '?ext_id=&region_id=&building_type_id=&media_type_id=&page=1';

        window.location.href = window.location.origin + window.location.pathname + params;
    });

    $('.js-ad-medium-delete').on('click', function(e) {
        e.preventDefault();
        var self = $(this);
        bootbox.confirm({
            message: "Удалить рекламный носитель?",
            buttons: {
                confirm: {
                    label: 'Да',
                    className: 'btn-sm btn-brand'
                },
                cancel: {
                    label: 'Нет',
                    className: 'btn-sm btn-light'
                }
            },
            callback: function (result) {
                if (result == true) {
                    self.next('form').submit();
                } else {
                    return;
                }
            }
        });
    });
}

// Форма (создание, редактирование)
if ($('#ad-medium-form-block').length == 1) {

    var files = {};
    var fileExt = ['jpg', 'jpeg', 'png', 'tiff', 'bmp'];

    var $imagesGrid = $('.images-list').masonry({
        itemSelector: '.image-item',
        columnWidth: 240,
        gutter: 10
    });

    $imagesGrid.imagesLoaded().progress( function() {
        $imagesGrid.masonry('layout');
    });

    $('#ad-medium-form').on('click', '#js-btn-save', function(e) {
        e.preventDefault();

        var form = $('#ad-medium-form');
        var formData = new FormData(form[0]);
        var errorsBlock = $('.errors');

        errorsBlock.html('');

        if (!$.isEmptyObject(files)) {
            for (var key in files) {
                formData.append('files['+key+']', files[key]);
            }
        }

        $('#js-btn-save').addClass('disabled');

        $.ajax({
            url: '/ad-medium/ajax-validate',
            type: 'POST',
            data: formData,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            success: function (result) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: formData,
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        setTimeout(function(){
                            $('#js-btn-save').removeClass('disabled');
                            window.location.href = window.location.origin + '/ad-medium/list';
                        }, 500);
                    },
                    error: function (err) {

                        setTimeout(function(){
                            $('#js-btn-save').removeClass('disabled');
                            var tpl = _.template($('#js-validation-errors').html());
                            errorsBlock.html(tpl({ items: {server:['Ошибка сервера.']} }));
                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                        }, 500);
                    }
                });
            },
            error: function (err) {
                setTimeout(function(){
                    $('#js-btn-save').removeClass('disabled');
                    var tpl = _.template($('#js-validation-errors').html());
                    if (err.status == 422)
                        errorsBlock.html(tpl({ items: err.responseJSON.errors}));
                    else
                        errorsBlock.html(tpl({ items: {server:['Ошибка сервера.']} }));
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }, 500);
            }
        });
    });

    // Добавление файла
    $('#js-add-file').click(function(e){
        e.preventDefault();
        $('input[type=file]').trigger('click');
    });

    $(document).on('change', 'input[type=file]', function() {

        if ($(this).get(0).files.length == 0)
            return false;

        var errorsBlock = $('.errors');
        var tpl = _.template($('#js-validation-errors').html());

        errorsBlock.html('');
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            if (($(this).get(0).files[i].size / 1024) > 10000) {
                errorsBlock.html(tpl({ items: {file:['Максимальный объем файла 10Mb.']} }));
                return false;
            }
            if (!fileExt.includes($(this).get(0).files[i].name.split('.').pop())) {
                errorsBlock.html(tpl({ items: {file:['Формат файла должен быть: jpg, jpeg, png, tiff, bmp']} }));
                return false;
            }
        }

        if (files == undefined)
            files = {};

        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var file = $(this).get(0).files[i];
            var reader = new FileReader();
            var tpl = _.template($('#js-image-item').html());
            if (files[file.name] == undefined) {
                reader.onload = function(e) {
                    var $item = $(tpl({ path: e.target.result, name: e.target.fileName }));
                    $imagesGrid.append($item).masonry('appended', $item);
                    $imagesGrid.imagesLoaded().progress( function() {
                        $imagesGrid.masonry('layout');
                    });
                }
                reader.fileName = file.name;
                reader.readAsDataURL(file);
                files[file.name] = file;
            }
        }

        $(this).val('');
    });

    // Удаление файла
    $(document).on('click', '.image-item span', function(){

        var parent = $(this).parent('.image-item')[0];
        $imagesGrid.masonry('remove', $(parent)).masonry('layout');

        if ($(this).data('id') != '') {
            var deleteFiles = JSON.parse($('input[name="delete_files"]').val());
            deleteFiles.push($(this).data('id'));
            $('input[name="delete_files"]').val(JSON.stringify(deleteFiles));
            $(this).parents('.image-item').remove();
        } else {
            var fileName = $(this).data('name');
            if (files[fileName] != undefined)
                delete files[fileName];
            $(this).parents('.image-item').remove();
        }
    });

    // Просмотр файла
    $(document).on('click', '.image-item img', function(e) {
        var span = $(this).next();
        if (e.target.localName == 'img' && span.data('path') != '') {
            window.open(window.location.origin + span.data('path'), '_blank');
        }
    });

    // Карта
    if ($('#map').length == 1) {

        ymaps.ready(init);
    
        function init() {

            var placemark;
            var map = new ymaps.Map("map", {
                center: [55.7558, 37.6173], // latitude, longitude
                controls: ['zoomControl', 'typeSelector'],
                zoom: 10
            });

            map.behaviors.disable('scrollZoom');
            map.behaviors.disable('dblClickZoom');

            if ($('input[name="map_lon"]').val() != '' && $('input[name="map_lat"]').val() != '')
            {
                var coords = [$('input[name="map_lat"]').val(), $('input[name="map_lon"]').val()];
                placemark = new ymaps.Placemark(coords, {
                    hintContent: $('textarea[name="address"]').val()
                }, {
                    preset: 'islands#dotIcon',
                    iconColor: '#3c52f6'
                });
                map.geoObjects.add(placemark);
                map.setZoom(12);
                map.setCenter(coords);
            }

            // Выбрать регион
            $('select[name="region_id"]').on('change', function(e) {
                var regionId = $(this).val();
                var regionTitle = (regionId != '') ? $(this).find(":selected").text() : 'Москва';

                ymaps.geocode(regionTitle, {
                    results: 1
                }).then(function (res) {
                    var coords = res.geoObjects.get(0).geometry.getCoordinates();
                    $('input[name="map_lon"]').val('');
                    $('input[name="map_lat"]').val('');
                    $('textarea[name="address"]').val('');
                    placemark = undefined;
                    map.geoObjects.removeAll();
                    map.setCenter(coords);
                });
            });

            // Клик [найти]
            $('#js-find-address').on('click', function(e) {
                e.preventDefault();
                var region = $('select[name="region_id"]').find(":selected").text();
                var address = $('textarea[name="address"]').val();

                if (region != '' && address != '') {
                    var location = region + ', ' + address;

                    ymaps.geocode(location, {
                        results: 1
                    }).then(function (res) {
                        var firstGeoObject = res.geoObjects.get(0);
                        var coords = firstGeoObject.geometry.getCoordinates();

                        if (placemark) {
                            placemark.geometry.setCoordinates(coords);
                            $('input[name="map_lon"]').val(coords[1]);
                            $('input[name="map_lat"]').val(coords[0]);
                        } else {
                            placemark = new ymaps.Placemark(coords, {}, {
                                preset: 'islands#dotIcon',
                                iconColor: '#3c52f6',
                                draggable: true
                            });
                            placemark.events.add('dragend', function () {
                                getAddress(placemark.geometry.getCoordinates());
                            });
                        }

                        map.geoObjects.add(placemark);
                        map.setBounds(map.geoObjects.getBounds(), {
                            checkZoomRange: true,
                        });
                    });
                }
            });

            // Слушаем клик на карте.
            map.events.add('click', function (e) {

                if ($('select[name="region_id"]').val() == '') 
                    return false;

                var coords = e.get('coords');

                if (placemark) {
                    placemark.geometry.setCoordinates(coords);
                } else {
                    placemark = new ymaps.Placemark(coords, {}, {
                        preset: 'islands#dotIcon',
                        iconColor: '#3c52f6',
                        draggable: true
                    });
                    map.geoObjects.add(placemark);
                    // Слушаем событие окончания перетаскивания на метке.
                    placemark.events.add('dragend', function () {
                        getAddress(placemark.geometry.getCoordinates());
                    });
                }
                getAddress(coords);
            });

            // Определяем адрес по координатам (обратное геокодирование).
            function getAddress(coords) {
                ymaps.geocode(coords).then(function (res) {
                    var firstGeoObject = res.geoObjects.get(0);
                    var coords = firstGeoObject.geometry.getCoordinates()

                    $('input[name="map_lon"]').val(coords[1]);
                    $('input[name="map_lat"]').val(coords[0]);
                    $('textarea[name="address"]').val(firstGeoObject.properties.get('name'));

                    placemark.properties.set({
                        hintContent: firstGeoObject.properties.get('name')
                    });
                });
            }
        }
    }
}
/**
 * Карта КП
 * 
 * Функционал карты:
 * - поиск по радиусу (установка точки на карте)
 * - поиск по произвольной области (АДТ Москвы и рисование через ALT)
 */
if ($('#commercial-offer-block').length == 1) {
    var csrf = $("meta[name=csrf_token]").attr('content');
    // Тип поиска: radius, polygon. От этого параметра зависит результат на карте.
    var searchType = 'radius';
    // Геоданные по АТД Москвы (https://gis-lab.info/qa/moscow-atd.html)
    var mscRegions = require('../ext/msc_ad.json');

    // Модуль рисования на карте
    require('../ext/paint_on_map.js');

    ymaps.ready(['ext.paintOnMap'])
         .then(initMap)
         .catch(console.error);

    function initMap() {
        var isResizing = false;
        var hasObjects = false;
        var tmpPlacemark;
        var map = new ymaps.Map("map", {
            center: [55.7558, 37.6173], // latitude, longitude
            controls: ['fullscreenControl', 'zoomControl', 'typeSelector'],
            zoom: 10
        });
        var paintProcess;
        var polygonStyles = {
            strokeColor: '#8593f977',
            fillColor: '#8593f977',
            strokeWidth: 2
        };
        var polygonCoords = [];
        var ListBoxLayout = ymaps.templateLayoutFactory.createClass(
            "<div id='js-msc-ad'>{{data.title}}</div>" +
            "<ul id='msc-ad-list' class='{% if state.expanded %}{% else %}invisible{% endif %}'></ul>", {
            build: function() {
                ListBoxLayout.superclass.build.call(this);
                this.childContainerElement = $('#msc-ad-list').get(0);
                this.events.fire('childcontainerchange', {
                    newChildContainerElement: this.childContainerElement,
                    oldChildContainerElement: null
                });
                if (searchType != 'polygon') $('#js-msc-ad').addClass('invisible');
            },
            getChildContainerElement: function () {
                return this.childContainerElement;
            },
            clear: function () {
                this.events.fire('childcontainerchange', {
                    newChildContainerElement: null,
                    oldChildContainerElement: this.childContainerElement
                });
                this.childContainerElement = null;
                ListBoxLayout.superclass.clear.call(this);
            }
        });
        var listBoxItems = function() {
            var a = [];
            if (mscRegions.length > 0) {
                for (var i =0; i < mscRegions.length; i++) {
                    a.push(new ymaps.control.ListBoxItem({data: {
                        title: i+1 + '. ' + mscRegions[i]['title'],
                        id: i+1
                    }}));
                }
            }
            return a;
        };
        var listBox = new ymaps.control.ListBox({
            items: listBoxItems(),
            data: {title: 'Административный округ'},
            options: {
                layout: ListBoxLayout,
                itemLayout: ymaps.templateLayoutFactory.createClass('<li data-id="{{data.id}}">{{data.title}}</li>')
            }
        });

        // Добавление АТД Москвы
        listBox.events.add('click', function (e) {
            var item = e.get('target');

            if (item != listBox
                && !checkPolygonCoords(item.data.get('id'))
                && !$('#msc-ad-list li[data-id="' + item.data.get('id') + '"]').hasClass('active')) {
                var id = item.data.get('id');
                var coords = mscRegions[id-1]['coords'];

                if (coords.length > 1) {
                    // Добавление связки полигонов определенного округа с единым id
                    for (var i = 0; i < coords.length; i++) {
                        addPolygon(coords[i][0], id);
                    }
                } else {
                    addPolygon(coords[0], id);
                }

                $('#msc-ad-list li[data-id="' + id + '"]').addClass('active');
            }
        });
        
        map.controls.add(listBox, {float: 'left'});

        map.behaviors.disable('scrollZoom');
        map.behaviors.disable('dblClickZoom');

        // Отслеживаем клик на карте.
        map.events.add('click', function (e) {
            if ($('select[name="region_id"]').val() == '')
            {
                return false;
            }

            var coords = e.get('coords');
            // Если метка уже создана - просто передвигаем ее.
            if (tmpPlacemark) {
                tmpPlacemark.geometry.setCoordinates(coords);
            } else {
                // Если нет - создаем.
                tmpPlacemark = new ymaps.Placemark(coords, {
                    type: 'placemark'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: '/img/placemark_parent_tmp.png',
                    iconImageSize: [22, 22],
                    iconImageOffset: [-11, -11],
                    draggable: true
                });
                map.geoObjects.add(tmpPlacemark);
                hasObjects = true;
                // Слушаем событие окончания перетаскивания на метке.
                tmpPlacemark.events.add('dragend', function () {
                    getAddress(tmpPlacemark.geometry.getCoordinates());
                });
                tmpPlacemark.events.add('click', function (e) {
                    var address = e.get('target').properties.get('hintContent');
                    var addressesList = $('#addresses-list textarea').val().split('\n');

                    if (addressesList.length == 1 && addressesList[0] == '') {
                        $('#addresses-list textarea').val($('#addresses-list textarea').val()+address);
                    } else {
                        $('#addresses-list textarea').val($('#addresses-list textarea').val()+'\n'+address);
                    }

                    $('#addresses-list a').trigger('click');
                });
            }

            getAddress(coords);
        });

        // Отслеживаем событие нажатия кнопки мыши.
        map.events.add('mousedown', function (e) {
            // Если кнопка мыши была нажата с зажатой клавишей "alt", то начинаем рисование контура.
            if (e.get('altKey') && searchType == 'polygon') {
                paintProcess = ymaps.ext.paintOnMap(map, e, {style: polygonStyles});
            }
        });

        // Отслеживаем событие отпускания кнопки мыши.
        map.events.add('mouseup', function (e) {
            if (paintProcess && searchType == 'polygon') {
                var polygon, coordinates;
                coordinates = paintProcess.finishPaintingAt(e);
                paintProcess = null;
                addPolygon(coordinates, genPolygonRandId());
            }
        });

        // Выбрать регион
        $('select[name="region_id"]').on('change', function(e) {
            var regionId = $(this).val();
            var regionTitle = $(this).find(":selected").text();

            if (regionId == '') {
                regionTitle = 'Москва';
                $('#js-reset-map').trigger('click');
                $('.map-bottom-panel').addClass('d-none');
            } else {
                $('.map-bottom-panel').removeClass('d-none');
            }

            if (searchType == 'polygon'
                && regionTitle == 'Москва'
                && $('#js-msc-ad').hasClass('invisible')) {
                $('#js-msc-ad').removeClass('invisible');
            } else {
                $('#js-msc-ad').addClass('invisible');
            }

            ymaps.geocode(regionTitle, {
                results: 1
            }).then(function (res) {
                var coords = res.geoObjects.get(0).geometry.getCoordinates();
                $('#js-reset-map').trigger('click');
                map.setCenter(coords);
            });
        });

        // Добавить адрес в список
        $('#js-add-addreses').on('click', function(e){
            e.preventDefault();
            $('#addresses-list').toggleClass('invisible');
        });
        $('#js-addresses-list-close').on('click', function(e){
            e.preventDefault();
            $('#addresses-list').addClass('invisible');
        });

        // Добавить адрес на карту из списка
        $('#addresses-list a#js-add-addresses').on('click', function(e){
            e.preventDefault();
            var addressesList = $('#addresses-list textarea').val().split('\n');
            var regionTitle = $('select[name="region_id"]').find(":selected").text();

            $('#addresses-list').addClass('invisible');

            var list = [];
            for (var i = 0; i < addressesList.length; i++) {
                if (addressesList[i] != '') {
                    list.push(addressesList[i]);
                }
            }

            if (list.length > 0 && regionTitle != '') {

                $('input[name="addresses"]').val(JSON.stringify([]));
                map.geoObjects.remove(tmpPlacemark);
                tmpPlacemark = undefined;

                for (var i = 0; i < list.length; i++) {
                    if (list[i] != '') {
                        var location = regionTitle + ', ' + list[i];
                        ymaps.geocode(location, {
                            results: 1
                        }).then(function (res) {
                            var obj = res.geoObjects.get(0);
                            var addresses = JSON.parse($('input[name="addresses"]').val());

                            addresses.push({
                                coords: obj.geometry.getCoordinates(),
                                name: obj.properties.get('name')
                            });
 
                            $('input[name="addresses"]').val(JSON.stringify(addresses));

                            map.geoObjects.add(new ymaps.Placemark(obj.geometry.getCoordinates(), {
                                hintContent: obj.properties.get('name'),
                                type: 'placemark'
                            }, {
                                iconLayout: 'default#image',
                                iconImageHref: '/img/placemark_parent.png',
                                iconImageSize: [22, 22],
                                iconImageOffset: [-11, -11],
                            }));

                            hasObjects = true;
                        });
                    }
                }
            }
        });

        // Табы режима поиска
        $('.search-type-tabs li').on('click', function(){
            if ($(this).hasClass('active')) {
                return false;
            }

            var hash = $(this).data('hash');
            var switchTabs = function(hash) {

                var regionTitle = $('select[name="region_id"]').find(":selected").text();
                searchType = (hash == '#radius') ? 'radius' : 'polygon';

                $('.search-type-tabs li[data-hash="'+hash+'"]').siblings().removeClass('active');
                $('.search-type-tabs li[data-hash="'+hash+'"]').addClass('active');
    
                $('.search-type-content '+hash).siblings().removeClass('active');
                $('.search-type-content '+hash).addClass('active');
    
                $('#js-reset-map').trigger('click');

                if (searchType == 'polygon'
                    && regionTitle == 'Москва'
                    && $('#js-msc-ad').hasClass('invisible')) {
                    $('#js-msc-ad').removeClass('invisible');
                } else {
                    $('#js-msc-ad').addClass('invisible');
                }
            };

            if (hasObjects) {
                bootbox.confirm({
                    message: 'На карте есть объекты, если сменить режим поиска - они будут удалены. Хотите продолжить?',
                    buttons: {
                        confirm: { label: 'Да', className: 'btn-sm btn-brand' },
                        cancel: { label: 'Нет', className: 'btn-sm btn-light' }
                    },
                    callback: function (result) {
                        if (result == true) {
                            switchTabs(hash);
                        } else {
                            return;
                        }
                    }
                });
            } else {
                switchTabs(hash);
            }
        });

        // Удалить адреса с карты (очистка параметров)
        $('#js-reset-map').on('click', function(e){
            e.preventDefault();

            $.ajax({
                url: '/shared/ajax-forget-map-result',
                type: 'POST',
                data: { _token: csrf },
                dataType: 'JSON',
                success: function (result) {
                    $('#addresses-list').addClass('invisible');
                    $('#addresses-list textarea').val('');
                    $('input[name="addresses"]').val(JSON.stringify([]));
                    $('input[name="radius"]').val(0);
                    $('input[type=checkbox]').prop('checked', false);
                    $('#msc-ad-list li').removeClass('active');
                    $('.map-result').empty();
         
                    polygonCoords = [];
                    tmpPlacemark = undefined;
                    hasObjects = false;
        
                    map.geoObjects.removeAll();
                    map.setZoom(10);

                    // Разблокировать форму заявки
                    $('#feedback-form input').attr("disabled", false);
                    $('#feedback-form .switch-block-1').removeClass('d-none');
                    $('#feedback-form .switch-block-2').addClass('d-none');
                },
                error: function (err) {
                    console.log(err);
                }
            });
        });

        // Поиск по карте с выбранными параметрами
        $('.map-bottom-panel a').on('click', function(e){
            e.preventDefault();

            var link = $(this);
            var regionId = $('select[name="region_id"]').val();
            var regionTitle = $('select[name="region_id"]').find(":selected").text();
            var radius = $('input[name="radius"]').val();
            var buildingTypeIds = [];
            var mediaTypeIds = [];
            var addresses = JSON.parse($('input[name="addresses"]').val());

            if (link.hasClass('disabled')) {
                return false;
            }

            if (searchType == 'radius' && radius == 0
                || searchType == 'radius' && addresses.length == 0) {
                return false;
            }

            if (searchType == 'polygon' && polygonCoords.length == 0) {
                return false;
            }

            $('.building-type-checkbox:checked').each(function (i) {
                buildingTypeIds[i] = $(this).val();
            });

            $('.media-type-checkbox:checked').each(function (i) {
                mediaTypeIds[i] = $(this).val();
            });

            // Подготовка координат полигонов
            if (polygonCoords.length > 0) {
                var a = [];
                for (var i = 0; i < polygonCoords.length; i++) {
                    a.push(polygonCoords[i]['coords']);
                }
                polygonCoords = a;
            }
            // !Делаем их строкой иначе от большого объема массива виснет браузер
            polygonCoords = JSON.stringify(polygonCoords);

            link.addClass('disabled');

            $.ajax({
                url: '/shared/ajax-calc-map-points',
                type: 'POST',
                data: {
                    _token: csrf,
                    search_type: searchType,
                    region_id: regionId,
                    filters: {
                        radius: radius,
                        polygon_coords: polygonCoords,
                        building_type_ids: buildingTypeIds,
                        media_type_ids: mediaTypeIds,
                    }, 
                    addresses: addresses
                },
                dataType: 'JSON',
                success: function (result) {
                    var tpl = _.template($('#js-map-result').html());

                    map.geoObjects.removeAll();
                    polygonCoords = [];

                    (searchType == 'radius') ? initByRadius(result, radius) : initByPolygon(result);

                    $('.map-result').html(tpl({
                        search_type: searchType,
                        region_title: regionTitle,
                        radius: radius,
                        items: result.items,
                        items_info: result.items_info
                    }));

                    link.removeClass('disabled');
                },
                error: function (err) {
                    console.log(err);
                    link.removeClass('disabled');
                }
            });
        });

        // Изменить высоту блока карты
        $('.drag-map').on('mousedown', function (e) {
            isResizing = true;
        });
        $(document).on('mousemove', function (e) {
            if (isResizing && e.clientY > $('#map').data('fixed-height')) {
                $('#map').css({'height': e.clientY + 'px'});
                map.container.fitToViewport();
            }
        }).on('mouseup', function (e) {
            isResizing = false;
        });

        // Удалить точку по клику из таблицы
        $(document).on('click', '.js-remove-point', function (e) {
            e.preventDefault();

            var id = $(this).data('id');
            var radius = $('input[name="radius"]').val();

            map.geoObjects.each(function (el) {
                if (el.properties.get('placemarkId') != undefined && el.properties.get('placemarkId') == id) {
                    removePointFromMap(el, radius)
                }
            });
        });

        // Определить адрес по координатам (обратное геокодирование)
        function getAddress(coords) {
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);

                tmpPlacemark.properties.set({
                    hintContent: firstGeoObject.properties.get('name')
                });
            });
        }

        // Добавить точки по радиусу
        function initByRadius(result, radius) {
            var items = result.items;

            for (var i = 0; i < items.length; i++) {
                // Установка круга
                map.geoObjects.add(new ymaps.Circle([items[i]['address']['coords'], radius], {
                    type: 'circle'
                }, {
                    fillColor: "#8593f977",
                    strokeColor: "#8593f9",
                }));
                // Установка центральной метки
                map.geoObjects.add(new ymaps.Placemark(items[i]['address']['coords'], {
                    hintContent: items[i]['address']['name'],
                    type: 'placemark'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: '/img/placemark_parent.png',
                    iconImageSize: [22, 22],
                    iconImageOffset: [-11, -11],
                }));
                // Установка меток находящихся в радиусе круга
                if (items[i]['points'].length > 0) {
                    for (var j = 0; j < items[i]['points'].length; j++) {
                        var p = items[i]['points'][j];
                        var placemark = new ymaps.Placemark([p['map_lat'], p['map_lon']], {
                            hintContent: p['address'],
                            type: 'placemark',
                            placemarkId: p['id'],
                            title1: p['building_type']['title'] +' '+ p['title'] +' '+'('+ p['address'] +')',
                            title2: ((p['media_type'] != null) ? p['media_type']['title'] + ', ' : '') + ((p['size'] != null) ? p['size'] + ', ' : '') + ((p['orientation'] != null) ? p['orientation'] : '')
                        }, {
                            iconLayout: 'default#image',
                            iconImageHref: '/img/placemark_child.png',
                            iconImageSize: [12, 12],
                            iconImageOffset: [-6, -6]
                        });
                        placemark.events.add('click', function (e) {
                            removePointFromMap(e.get('target'), radius);
                        });
                        map.geoObjects.add(placemark);
                    }
                }

                map.setBounds(map.geoObjects.getBounds(), {checkZoomRange: true});
                hasObjects = true;
            }
        }

        // Добавить точки по полигону
        function initByPolygon(result) {
            var items = result.items;

            for (var i = 0; i < items.length; i++) {
                // Установка полигона
                map.geoObjects.add(new ymaps.Polygon([items[i]['polygon_coords']], {type: 'polygon', polygonId: null}, polygonStyles));
                polygonCoords.push({id: genPolygonRandId(), coords: items[i]['polygon_coords']});
                // Установка адресов (опционально)
                // В отличии от круга, адреса в полигоне необязательны
                if (i == 0 && items[i]['addresses'].length > 0) {
                    for (var j = 0; j < items[i]['addresses'].length; j++) {
                        map.geoObjects.add(new ymaps.Placemark(items[i]['addresses'][j]['coords'], {
                            hintContent: items[i]['addresses'][j]['name'],
                            type: 'placemark'
                        }, {
                            iconLayout: 'default#image',
                            iconImageHref: '/img/placemark_parent.png',
                            iconImageSize: [22, 22],
                            iconImageOffset: [-11, -11],
                        }));
                    }
                }
                // Установка меток находящихся в полигоне
                if (items[i]['points'].length > 0) {
                    for (var j = 0; j < items[i]['points'].length; j++) {
                        var p = items[i]['points'][j];
                        var placemark = new ymaps.Placemark([p['map_lat'], p['map_lon']], {
                            hintContent: p['address'],
                            type: 'placemark',
                            placemarkId: p['id'],
                            title1: p['building_type']['title'] +' '+ p['title'] +' '+'('+ p['address'] +')',
                            title2: ((p['media_type'] != null) ? p['media_type']['title'] + ', ' : '') + ((p['size'] != null) ? p['size'] + ', ' : '') + ((p['orientation'] != null) ? p['orientation'] : '')
                        }, {
                            iconLayout: 'default#image',
                            iconImageHref: '/img/placemark_child.png',
                            iconImageSize: [12, 12],
                            iconImageOffset: [-6, -6]
                        });
                        placemark.events.add('click', function (e) {
                            removePointFromMap(e.get('target'), radius);
                        });
                        map.geoObjects.add(placemark);
                    }
                }

                map.setBounds(map.geoObjects.getBounds(), {checkZoomRange: true});
                hasObjects = true;
            }
        }

        // Удалить точку на карте + в БД
        function removePointFromMap(target, radius) {
            bootbox.confirm({
                message: 'Удалить адрес?<br/><small class="removed-point-info text-muted">'+ target.properties.get('title1') + '<br/>' + target.properties.get('title2') + '</small>',
                buttons: {
                    confirm: { label: 'Да', className: 'btn-sm btn-brand' },
                    cancel: { label: 'Нет', className: 'btn-sm btn-light' }
                },
                callback: function (result) {
                    if (result == true) {
                        var object = target;
                        var placemarkId = object.properties.get('placemarkId');
                        var uid = $('input[name="uid"]').val();
            
                        if (uid != undefined && uid.match(/(.*)?uid=(.*)$/g) != null) {
                            uid = uid.replace(/(.*)?uid=/g, '');
                        }

                        $.ajax({
                            url: '/shared/ajax-remove-map-point',
                            type: 'POST',
                            data: {
                                _token: csrf,
                                id: placemarkId,
                                uid: uid
                            },
                            dataType: 'JSON',
                            success: function (result) {
                                // Удалить точку в таблице результата
                                var ul = $('.map-result-table li[data-id="' + placemarkId + '"]').parent();
                                $('.map-result-table li[data-id="' + placemarkId + '"]').remove();
                                if (ul.find('li').length == 0) {
                                    if (searchType == 'radius')
                                        ul.append('<li class="text-muted">Рекламные носители в радиусе ' + radius + 'м не найдены</li>');
                                    else
                                        ul.append('<li class="text-muted">Рекламные носители не найдены</li>');
                                }
                                $('.map-result-table').prev().text(result.items_info.title);
                                // Удалить точку на карте
                                map.geoObjects.remove(object);
                            },
                            error: function (err) {
                                console.log(err);
                            }
                        });
                    } else {
                        return;
                    }
                }
            });
        }

        // Добавить полигон на карту
        function addPolygon(coordinates, polygonId) {
            polygon = new ymaps.Polygon(
                [coordinates],
                {type: 'polygon', polygonId: polygonId},
                polygonStyles
            );

            // Отслеживаем клик по полигону
            polygon.events.add('click', function (e) {
                var id = e.get('target').properties.get('polygonId');
                var deletedId = null;
                var deletedEls = [];

                map.geoObjects.each(function(el){
                    if (el.properties.get('type') == 'polygon'
                        && el.properties.get('polygonId') == id
                        && checkPolygonCoords(el.properties.get('polygonId'))) {
                        deletedId = id;
                        deletedEls.push(el);
                    }
                });

                // Удаляем полигон
                if (deletedId != null) {
                    // АТД Москвы имеют одинаковый id по этому прогоняем в цикле
                    for (var i = 0; i < deletedEls.length; i++) {
                        map.geoObjects.remove(deletedEls[i]);
                    }
                    removePolygonCoords(deletedId);
                    $('#msc-ad-list li[data-id="' + deletedId + '"]').removeClass('active');
                }
            });

            map.geoObjects.add(polygon);
            polygonCoords.push({id: polygonId, coords: coordinates});
            hasObjects = true;
        }

        // Временный id полигона для массива polygonCoords
        // Полигоны из mscRegions имеют id [1,2,...], остальные имеют id [1601494117724,...]
        function genPolygonRandId() {
            return Date.now();
        }

        // Исключением удаленного по клику полигона
        function removePolygonCoords(polygonId) {
            if (polygonCoords.length > 0) {
                var a = [];
                for (var i = 0; i < polygonCoords.length; i++) {
                    if (polygonCoords[i]['id'] != polygonId) {
                        a.push(polygonCoords[i]);
                    }
                }
                polygonCoords = a;
            } else {
                polygonCoords = [];
            }
        }

        // Проверка полигона по его id
        function checkPolygonCoords(polygonId) {
            if (polygonCoords.length > 0) {
                for (var i = 0; i < polygonCoords.length; i++) {
                    if (polygonCoords[i]['id'] == polygonId) {
                        return true;
                    }
                }
            }
            return false
        }
    }

    // Гиперссылка
    $(document).on('click', '#js-get-link', function(e){
        e.preventDefault();

        var input = $(document).find('.map-result-top-pannel-2 input');
        $(document).find('.map-result-top-pannel-2').removeClass('d-none');

        if (input.val() != '') {
            return false;
        }

        $.ajax({
            url: '/shared/ajax-save-map-result',
            type: 'POST',
            data: { _token: csrf },
            dataType: 'JSON',
            success: function (result) {
                if (result.uid != null) {
                    input.val(result.link);
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    // Копировать гиперссылку
    $(document).on('click', '#js-copy-link', function(){
        var input = $(document).find('.map-result-top-pannel-2 input');
        if (input.val() != '') {
            input.select();
            document.execCommand("copy");
        }
    });

    // Экспорт excel
    $(document).on('click', '#js-export', function(e){
        e.preventDefault();

        var path = window.location.origin + '/shared/excel-export?name=';
        var fileName = $(document).find('input[name="excel_name"]').val();

        if (fileName != '') path += fileName.trim();
        $(document).find('#export-modal').modal('hide');

        setTimeout(function(){
            window.open(path, '_blank');
        }, 800);
    });

    $(document).on('hidden.bs.modal', '#export-modal', function (e) {
        $(this).find('input[name="excel_name"]').val('');
    });

    // Форма обратной связи
    $(document).on('change', '#feedback-form input', function (e) {
        e.preventDefault();

        if ($(this).val() != '') {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });

    $(document).on('submit', '#feedback-form', function (e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        var uid = $('input[name="uid"]').val();

        if (uid != undefined && uid.match(/(.*)?uid=(.*)$/g) != null) {
            uid = uid.replace(/(.*)?uid=/g, '');
        } else {
            uid = '';
        }

        formData.append('uid', uid);
        formData.append('_token', csrf);

        $('#js-send-feedback').addClass('disabled');
        $('#feedback-form input').removeClass('has-error');
        $('#feedback-form .custom-control-label').removeClass('has-error');

        $.ajax({
            url: '/shared/ajax-send-feedback',
            type: 'POST',
            data: formData,
            dataType: 'JSON',
            contentType: false,
            processData: false,
            success: function (result) {
                $('#js-send-feedback').removeClass('disabled');
                $('#feedback-form input').removeClass('not-empty');
                $('#feedback-form input').removeClass('has-error');
                $('#feedback-form .custom-control-label').removeClass('has-error');
                $('#feedback-form')[0].reset();
                if (result.uid != null) {
                    $('input[name="uid"]').val(result.link);
                }
                $('#feedback-form input').attr("disabled", true);
                $('#feedback-form .switch-block-1').addClass('d-none');
                $('#feedback-form .switch-block-2').removeClass('d-none');
            },
            error: function (err) {
                $('#js-send-feedback').removeClass('disabled');
                if (err.status == 422) {
                    for (var key in err.responseJSON.errors) {
                        if (key == 'personal_data') {
                            $('#feedback-form').find('.custom-control-label').addClass('has-error');
                        } else {
                            $('#feedback-form').find('input[name="'+key+'"]').addClass('has-error');
                        }
                    }
                } else {
                    alert('Ошибка сервера');
                    console.log(err);
                }
            }
        });
    });
}
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Заявка</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body >

<h2>Заявка</h2>
<b>Имя:</b> {{ $form['name'] }}<br/>
<b>Компания:</b> {{ $form['company'] }}<br/>
<b>Телефон:</b> {{ $form['phone'] }}<br/>
<b>Дата:</b> {{ date('Y-m-d H:i:s') }}<br/>

@if (!empty($map_link))
<br/>Результат на <a href="{{ $map_link }}" target="_blank">карте</a>
@endif

</body>
</html>
<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('libs/fontawesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css?v=13') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }}">
        <title>CLUMBA | @yield('title')</title>
    </head>
    <body>

        <div class="container-fluid">

            <aside id="sidebar">
                <div class="logo">
                    <a href="https://clumba.ru" target="_blank">
                        <img src="{{ asset('img/logo.svg') }}" />
                    </a>
                </div>
                @auth
                    <ul class="menu">
                        @foreach ($menu as $m)
                            @if(array_key_exists('sub_menu', $m))
                                <li class="{{ highlight_menu($m['path']) ? 'active menu-open' : '' }}">
                                    <a href="javascript:void(0)"><span>{{ $m['title'] }}</span> <i class="fa {{ highlight_menu($m['path']) ? 'fa-minus' : 'fa-plus'  }}"></i></a>
                                    <ul class="sub-menu" style="{{ highlight_menu($m['path']) ? 'display:block;' : 'display:none;'  }}">
                                        @foreach ($m['sub_menu'] as $sm)
                                            <li class="{{ highlight_submenu($sm['path']) ? 'active' : '' }}">
                                                <a href="{{ url($sm['path']) }}"><i class="far fa-circle"></i> <span>{{ $sm['title'] }}</span></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li class="{{ highlight_menu($m['path']) ? 'active' : '' }}">
                                    <a href="{{ url($m['path']) }}"><span>{{ $m['title'] }}</span></a>
                                </li>
                            @endif
                        @endforeach
                        <li>
                            <a href="javascript:void(0)" id="logout"><span>Выход</span></a>
                            <form action="{{ url('logout') }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                @endauth
                @guest
                    <div class="shared-contacts">
                        <span><b>+7 (495) 481 39 39</b></span>
                        <span>Москва, Грохольский переулок, 28</span>
                        <span><a href="mailto:sale@clumba.ru">sale@clumba.ru</a></span>
                    </div>
                @endguest
            </aside>

            <section id="content">

                  @yield('content')

            </section>

        </div>

        <script src="{{ asset('libs/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('libs/underscore-min.js') }}"></script>
        <script src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('libs/bootbox/bootbox.min.js') }}"></script>
        <script src="{{ asset('libs/masonry.pkgd.min.js') }}"></script>
        <script src="{{ asset('libs/imagesloaded.pkgd.min.js') }}"></script>
        <script src="https://api-maps.yandex.ru/2.1/?apikey={{ env('YANDEX_MAP_API_KEY') }}&lang=ru_RU" type="text/javascript"></script>
        <script src="{{ asset('js/app.js?v=16') }}"></script>
        
        @stack('scripts')

    </body>
</html>
@if (session('_flash_messages_'))
    @foreach (session('_flash_messages_') as $type => $messages)
        @foreach ($messages as $message)
            <div class="alert alert-{{ $type }} alert-dismissible fade show" role="alert">
                {!! $message !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endforeach
    @endforeach
@endif
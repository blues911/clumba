<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css?v=4') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }}">
        <title>CLUMBA | @yield('title')</title>
    </head>
    <body>

        <div class="container">

            @yield('content')

        </div>

    </body>
</html>
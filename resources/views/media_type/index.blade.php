@extends('layout.main')

@section('title')

    Справочники &ndash; Типы носителя

@endsection

@section('content')

    <h4 class="title" id="media-type-list-block">Справочники / Типы носителя</h4>

    @include('layout.inc._flash_messages')

    <a href="{{ url('dictionary/media-type/create') }}" class="btn btn-sm btn-brand mb-4"><i class="fa fa-plus"></i> Добавить</a>

    @if ($media_types)

        <table class="table">
            <thead>
                <tr>
                    <th scope="col" class="w100">№</th>
                    <th scope="col">Название</th>
                    <th scope="col" class="w100"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($media_types as $media_type)
                    <tr>
                        <th scope="row">{{ $params['number']++ }}</th>
                        <td>{{ $media_type->title }}</td>
                        <td>
                            <a href="{{ url('dictionary/media-type/edit/' . $media_type->id) }}" class="edit-item"><i class="fas fa-edit"></i></a>
                            <a href="#" class="delete-item js-media-type-delete"><i class="fas fa-trash-alt"></i></a>
                            <form action="{{ url('dictionary/media-type/delete/' . $media_type->id) }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $media_types->links() }}

    @else

        <p>Нет данных</p>

    @endif

@endsection
@extends('layout.main')

@section('title')

    {!! ($media_type) ? 'Справочники &ndash; Редактировать тип носителя' : 'Справочники &ndash; Добавить тип носителя' !!}

@endsection

@section('content')

    <h4 class="title" id="media-type-form-block">{{ ($media_type) ? 'Справочники / Редактировать тип носителя' : 'Справочники / Добавить тип носителя' }}</h4>

    @include('layout.inc._flash_messages')
    @include('layout.inc._validation_errors')

    <form id="media-type-form" class="edit-form" action="{{ ($media_type) ? url('dictionary/media-type/update/'.$media_type->id) : url('dictionary/media-type/store') }}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col col-sm-12 col-md-12 col-lg-12 form-group">
                <label class="custom-label">Название:</label>
                <input type="text" class="form-control" name="title" value="{{ ($media_type) ? $media_type->title : '' }}">
            </div>
        </div>
        <div class="row">
            <div class="col col-sm-12 col-md-12 col-lg-12 form-group">
                <label class="custom-label">Синонимы:</label>
                <textarea type="text" class="form-control" name="aliases" rows="7">{{ ($media_type) ? $media_type->aliases : '' }}</textarea>
            </div>
        </div>
        <hr/>
        <button type="submit" class="btn btn-brand">Сохранить</button>
        <a href="{{ url('/dictionary/media-type/list') }}" class="btn btn-outline-brand back">Назад</a>
    </form>

@endsection
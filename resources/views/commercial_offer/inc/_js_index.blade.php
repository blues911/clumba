<script id="js-map-result" type="text/template">
    <div class="map-result-top-pannel">
        <h6><%= region_title %></h6>
        <div>
            <a href="#" id="excel-export" data-toggle="modal" data-target="#export-modal">Экспортировать в Excel</a>
            <a href="#" id="js-get-link">Получить ссылку для клиента</a>
        </div>
    </div>
    <div class="map-result-top-pannel-2 d-none">
        <div class="input-group input-group-sm">
            <input type="text" name="uid" class="form-control" readonly>
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" id="js-copy-link" type="button"><i class="far fa-copy"></i></button>
            </div>
        </div>
    </div>
    <p><%= items_info.title %></p>
    <table class="table map-result-table">
        <tbody>
            <% if (search_type == 'radius') { %>
                <% _.each(items, function(item) { %>
                    <tr>
                        <td class="left"><span><%= item.address.name %></span></td>
                        <td class="right">
                            <% if (item.points.length > 0) { %>
                                <ul>
                                    <% _.each(item.points, function(point) { %>
                                        <li data-id="<%= point.id %>">
                                            <div>
                                                <span><%= point.building_type.title %> <%= point.title %> (<%= point.address %>) <span class="text-muted">~ <%= point.distance %>м</span></span>
                                                <span class="text-muted">Код: <%= point.ext_id %></span>
                                            </div>
                                            <div>
                                                <span><%= (point.media_type != null) ? point.media_type.title + ', ' : '' %><%= (point.size != null) ? point.size + ', ' : '' %><%= (point.orientation != null) ? point.orientation : '' %></span>
                                                <span><a href="#" class="js-remove-point" data-id="<%= point.id %>">удалить</a></span>
                                            </div>
                                        </li>
                                    <% }); %>
                                </ul>
                            <% } else { %>
                                <ul><li class="text-muted">Рекламные носители в радиусе <%= radius %>м не найдены</li></ul>
                            <% } %>
                        </td>
                    </tr>
                <% }); %>
            <% } else { %>
                <tr>
                    <td class="left">
                        <% if (items[0].addresses.length > 0) { %>
                            <% _.each(items[0].addresses, function(address) { %>
                                <span><%= address.name %></span>
                            <% }); %>
                        <% } %>
                    </td>
                    <td class="right">
                        <ul>
                            <% if (items_info.status == 1) { %>
                                <% _.each(items, function(item) { %>
                                    <% if (item.points.length > 0) { %>
                                        <% _.each(item.points, function(point) { %>
                                            <li data-id="<%= point.id %>">
                                                <div>
                                                    <span><%= point.building_type.title %> <%= point.title %> (<%= point.address %>)</span>
                                                    <span class="text-muted">Код: <%= point.ext_id %></span>
                                                </div>
                                                <div>
                                                    <span><%= (point.media_type != null) ? point.media_type.title + ', ' : '' %><%= (point.size != null) ? point.size + ', ' : '' %><%= (point.orientation != null) ? point.orientation : '' %></span>
                                                    <span><a href="#" class="js-remove-point" data-id="<%= point.id %>">удалить</a></span>
                                                </div>
                                            </li>
                                        <% }); %>
                                    <% } %>
                                <% }); %>
                            <% } else { %>
                                <li class="text-muted">Рекламные носители не найдены</li>
                            <% } %>
                        </ul>
                    </td>
                </tr>
            <% } %>
        </tbody>
    </table>
    <!-- The Modal -->
    <div class="modal fade" id="export-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Экспортировать в Excel</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form class="edit-form">
                        <div class="form-group">
                            <label class="custom-label">Имя файла:</label>
                            <input type="text" class="form-control" name="excel_name">
                            <small class="form-text text-muted"><i class="fa fa-info-circle"></i> Поле необязательно для заполнения.</small>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-brand" id="js-export">Экспорт</button>
                </div>
            </div>
        </div>
    </div>
</script>
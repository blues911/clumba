@extends('layout.main')

@section('title')

    Коммерческие предложения

@endsection

@section('content')

    <h4 class="title" id="commercial-offer-block">Коммерческие предложения</h4>

    <div class="map-top-panel">
        <span>
            <b>Регион:</b>&nbsp;
            <select class="custom-select custom-select-sm" name="region_id">
                <option value="">Выбрать регион</option>
                @if ($params['regions'])
                    @foreach ($params['regions'] as $region)
                        <option value="{{ $region->id }}">{{ $region->title }}</option>
                    @endforeach
                @endif
            </select>
        </span>
        <ul>
            <li><a href="#" id="js-reset-map">Убрать все адреса</a></li>
            <li><a href="#" class="btn btn-sm btn-outline-brand" id="js-add-addreses"><i class="fa fa-plus"></i> Добавить адреса</a></li>
        </ul>
    </div>

    <div class="map-wrapper">
        <div id="addresses-list" class="invisible">
            <div class="form-group">
                <span class="help">Введите адрес для добавления на карту<br/>(каждый адрес с новой строки)</span>
                <textarea class="form-control" name="addresses"></textarea>
            </div>
            <div class="form-group">
                <a href="#" id="js-add-addresses" class="btn btn-sm btn-brand">Добавить</a>
                <a href="#" id="js-addresses-list-close">Закрыть</a>
            </div>
        </div>
        <div id="map" style="width: 100%; height: 420px" data-fixed-height="420"></div>
        <div class="drag-map"></div>
        <small class="form-text text-muted"><i class="fa fa-info-circle"></i> Для поиска адреса на карте должен быть выбран «Регион»</small>
    </div>

    <div class="map-bottom-panel d-none">
        <input type="hidden" name="addresses" value="[]">
        <div class="row">
            <div class="col col-md-6 col-lg-6 col-xl-6 col-check">
                @if ($params['building_types'])
                      <div class="filters-block">
                          <div class="filters-block-title"><b>Тип локации:</b></div>
                          <div class="filters-block-content">
                              @foreach ($params['building_types'] as $building_type)
                                  <div class="custom-control custom-checkbox filters-checkbox">
                                      <input type="checkbox" name="building_type_ids[]" value="{{ $building_type->id }}" class="custom-control-input building-type-checkbox" id="building-type-check-{{ $building_type->id }}">
                                      <label class="custom-control-label" for="building-type-check-{{ $building_type->id }}">{{ $building_type->title }}</label>
                                  </div>
                              @endforeach
                          </div>
                      </div>
                  @endif
                  @if ($params['media_types'])
                      <div class="filters-block">
                          <div class="filters-block-title"><b>Тип носителя:</b></div>
                          <div class="filters-block-content">
                              @foreach ($params['media_types'] as $media_type)
                                <div class="custom-control custom-checkbox filters-checkbox">
                                    <input type="checkbox" name="media_type_ids[]" value="{{ $media_type->id }}" class="custom-control-input media-type-checkbox" id="media-type-check-{{ $media_type->id }}">
                                    <label class="custom-control-label" for="media-type-check-{{ $media_type->id }}">{{ $media_type->title }}</label>
                                </div>
                              @endforeach
                          </div>
                      </div>
                @endif
            </div>
            <div class="col col-md-4 col-lg-4 col-xl-4 col-search-type">
                <div class="search-type">
                    <ul class="search-type-tabs">
                        <li data-hash="#radius" class="active">Радиус поиска</li>
                        <li data-hash="#polygon">Произвольная область</li>
                    </ul>
                    <div class="search-type-content">
                        <div id="radius" class="content-block active">
                            <div class="content-block-top">
                                Задайте радиус поиска (в метрах)
                            </div>
                            <div class="content-block-bottom">
                                <input type="number" name="radius" class="form-control" value="0" min="0">
                            </div>
                        </div>
                        <div id="polygon" class="content-block">
                            <div class="content-block-top">
                                Зажмите ALT и выберите на карте произвольную область
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-md-2 col-lg-2 col-xl-2 col-btn">
                <div class="btn-block-content">
                    <a href="#" class="btn btn-lg btn-brand">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span class="text">Найти</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="map-result"></div>

    @guest
        <div class="map-request-form">
            <div class="top-title">
                Просчитаем рекламную кампанию
                <br/>на максимально выгодных условиях
            </div>
            <div class="row">
                <div class="col col-md-6 col-lg-6 col-xl-6">
                    <div class="employee-info">
                        <div><img src="{{ asset('img/person.png') }}" alt="Сабина Кузеняткина"></div>
                        <div>
                            <b>Сабина Кузеняткина</b>
                            <br/>Директор отдела продаж
                        </div>
                    </div>
                </div>
                <div class="col col-md-6 col-lg-6 col-xl-6">
                    <form id="feedback-form">
                        <p>Введите ваши контакты, и мы свяжемся с вами<br>для обсуждения деталей рекламной кампании:</p>
                        <p>Согласуем креативы, подготовим макеты, посчитаем медиаплан и запустим в трансляцию.</p>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control">
                            <label>Имя</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="company" class="form-control">
                            <label>Компания</label>
                        </div>
                        <div class="form-group">
                            <input type="tel" name="phone" class="form-control">
                            <label>Телефон</label>
                        </div>
                        <div class="switch-block-1">
                            <button type="submit" class="btn btn-brand" id="js-send-feedback">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                <span class="text">Оставить заявку</span>
                            </button>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="personal_data" class="custom-control-input" id="personal-data">
                                <label class="custom-control-label" for="personal-data">
                                    <span>Согласен на обработку персональных данных.</span>
                                    <br/><a href="https://clumba.ru/privacy/" target="_blank">Политика конфиденциальности</a>
                                </label>
                            </div>
                        </div>
                        <div class="switch-block-2 d-none"><p>Заявка успешно отправлена. Наши менеджеры свяжутся с вами.</p></div>
                    </form>
                </div>
            </div>
        </div>
    @endguest

    @include('commercial_offer.inc._js_index')

@endsection

@guest
    @push('scripts')
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
            ym(68370991, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/68370991" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    @endpush
@endguest
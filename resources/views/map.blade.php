<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('libs/fontawesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/map.css?v=3') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }}">
        <title>CLUMBA | Коммерческие предложения</title>
    </head>
    <body>

        <div id="content" class="container-fluid">
            <div class="header">
                <div class="row">
                    <div class="col-sm-4 col-md-3 col-lg-3">
                        <div class="logo">
                            <img src="{{ asset('img/logo.svg') }}" />
                            <span>All about<br>indoor</span>
                        </div>
                    </div>
                    <div class="col-md-6 map-filters">
                        @if (count($params['building_types']) > 0)
                            <div class="row building-type">
                                <div class="col-sm-4 col-md-3 col-lg-3">
                                    <strong>Тип локации:</strong>
                                </div>
                                <div class="col-sm-8 col-md-9 lg-9">
                                    @foreach ($params['building_types'] as $building_type)
                                        <label class="chx-container custom-checkbox">
                                            <input type="checkbox" class="building-type-checkbox" name="building_type_ids[]" value="{{ $building_type->id }}" {{ (in_array($building_type->id, $filters['building_type_ids'])) ? 'checked' : '' }}>
                                            <span class="chx-checkmark"></span>
                                            {{ $building_type->title }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        @if (count($params['media_types']) > 0)
                            <div class="row media-type">
                                <div class="col-sm-4 col-md-3 col-lg-3">
                                    <strong>Тип носителя:</strong>
                                </div>
                                <div class="col-sm-4 col-md-9 lg-9">
                                    @foreach ($params['media_types'] as $media_type)
                                        <label class="chx-container custom-checkbox">
                                            <input type="checkbox" class="media-type-checkbox" name="media_type_ids[]" value="{{ $media_type->id }}" {{ (in_array($media_type->id, $filters['media_type_ids'])) ? 'checked' : '' }}>
                                            <span class="chx-checkmark"></span>
                                            {{ $media_type->title }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <a href="https://clumba.ru/address/" target="_blank" class="address-program-btn">Полная адресная программа</a>
                    </div>
                </div>
            </div>
            <div class="map-wrapper">
                <div id="map" style="width: 100%; height: 640px;" data-content="[]" data-uid="{{ $params['uid'] }}" data-search-type="{{ $params['search_type'] }}"></div>
                <div id="placemark-info" data-content="">
                    <div class="placemark-info-content">
                        <img src="/img/map/modal_close.png" class="close">
                        <div class="placemark-info-title"></div>
                        <div class="placemark-info-address"></div>
                        <div class="placemark-info-params">
                            <div class="placemark-info-params-media-type"><span class="title">Тип носителя:</span> <span class="text"></span></div>
                            <div class="placemark-info-params-size"><span class="title">Размеры:</span> <span class="text" title=""></span></div>
                            <div class="placemark-info-params-orientation"><span class="title">Ориентация:</span> <span class="text"></span></div>
                            <div class="placemark-info-params-images">
                                <div class="images-grid"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="placemark-images">
                    <img src="/img/map/modal_close.png" class="close">
                    <img src="/img/map/modal_prev.png" class="prev" data-id="">
                    <img src="/img/map/modal_next.png" class="next" data-id="">
                    <span class="back">&larr; назад</span>
                    <div class="image-preview"></div>
                    <span class="total"></span>
                </div>
            </div>
        </div>

        <script src="https://api-maps.yandex.ru/2.1/?apikey={{ env('YANDEX_MAP_API_KEY') }}&lang=ru_RU" type="text/javascript"></script>
        <script src="{{ asset('libs/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('libs/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('libs/masonry.pkgd.min.js') }}"></script>
        <script src="{{ asset('libs/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ asset('js/map.js?v=5') }}"></script>

    </body>
</html>
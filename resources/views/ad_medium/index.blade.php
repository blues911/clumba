@extends('layout.main')

@section('title')

    Рекламные носители

@endsection

@section('content')

    <h4 class="title" id="ad-medium-list-block">Рекламные носители</h4>

    @include('layout.inc._flash_messages')

    <a href="{{ url('ad-medium/create') }}" class="btn btn-sm btn-brand"><i class="fa fa-plus"></i> Добавить</a>
    
    <div class="table-filters">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-3">
                <span>Код объекта в 1С:</span>
                <input type="text" name="ext_id" class="form-control form-control-sm" value="{{ $filters['ext_id'] }}"><br/>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3">
                <span>Регион:</span>
                <select class="custom-select custom-select-sm" name="region_id">
                    <option value="">Все</option>
                    @if ($params['regions'])
                        @foreach ($params['regions'] as $region)
                            <option value="{{ $region->id }}" @if ($filters['region_id'] == $region->id) selected @endif>{{ $region->title }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3">
                <span>Тип локации:</span>
                <select class="custom-select custom-select-sm" name="building_type_id">
                    <option value="">Все</option>
                    @if ($params['building_types'])
                        @foreach ($params['building_types'] as $building_type)
                            <option value="{{ $building_type->id }}" @if ($filters['building_type_id'] == $building_type->id) selected @endif>{{ $building_type->title }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3">
                <span>Тип носителя:</span>
                <select class="custom-select custom-select-sm" name="media_type_id">
                    <option value="">Все</option>
                    @if ($params['media_types'])
                        @foreach ($params['media_types'] as $media_type)
                            <option value="{{ $media_type->id }}" @if ($filters['media_type_id'] == $media_type->id) selected @endif>{{ $media_type->title }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <a href="#" class="btn btn-sm btn-outline-brand search"><i class="fa fa-search"></i> Найти</a>
                <a href="#" class="btn btn-sm btn-outline-brand reset"><i class="fa fa-times-circle"></i> Сбросить</a>
            </div>
        </div>
    </div>

    @if ($ad_mediums)

        <table class="table">
            <thead>
                <tr>
                    <th scope="col"class="w100">№</th>
                    <th scope="col">Код объекта в 1С</th>
                    <th scope="col">Регион</th>
                    <th scope="col">Тип локации</th>
                    <th scope="col">Тип носителя</th>
                    <th scope="col">Название</th>
                    <th scope="col" class="w100"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($ad_mediums as $ad_medium)
                    <tr>
                        <th scope="row">{{ $params['number']++ }}</th>
                        <td>{{ $ad_medium->ext_id }}</td>
                        <td>{{ $ad_medium->region->title }}</td>
                        <td>{{ $ad_medium->buildingType ? $ad_medium->buildingType->title : '' }}</td>
                        <td>{{ $ad_medium->mediaType ? $ad_medium->mediaType->title : '' }}</td>
                        <td>{{ $ad_medium->title }}</td>
                        <td>
                            <a href="{{ url('ad-medium/edit/'.$ad_medium->id) }}" class="edit-item"><i class="fas fa-edit"></i></a>
                            <a href="#" class="delete-item js-ad-medium-delete"><i class="fas fa-trash-alt"></i></a>
                            <form action="{{ url('ad-medium/delete/'.$ad_medium->id) }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $ad_mediums->links() }}

    @else

        <p>Нет данных</p>

    @endif

@endsection
@extends('layout.main')

@section('title')

    {{ ($ad_medium) ? 'Редактировать рекламный носитель' : 'Добавить рекламный носитель' }}

@endsection

@section('content')

    <h4 class="title" id="ad-medium-form-block">{{ ($ad_medium) ? 'Редактировать рекламный носитель' : 'Добавить рекламный носитель' }}</h4>

    @include('layout.inc._flash_messages')

    <div class="errors"></div>

    <form id="ad-medium-form" class="edit-form" action="{{ ($ad_medium) ? url('ad-medium/ajax-update/'.$ad_medium->id) : url('ad-medium/ajax-store') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" class="form-control" name="map_lat" value="{{ ($ad_medium) ? $ad_medium->map_lat : '' }}">
        <input type="hidden" class="form-control" name="map_lon" value="{{ ($ad_medium) ? $ad_medium->map_lon : '' }}">
        <div class="row">
            <div class="col col-sm-6 col-md-6 col-lg-6 form-group">
                <label class="custom-label">Код объекта в 1С:</label>
                <input type="text" class="form-control" name="ext_id" value="{{ ($ad_medium) ? $ad_medium->ext_id : '' }}">
            </div>
            <div class="col col-sm-12 col-md-6 col-lg-6 form-group">
                <label class="custom-label">Регион:</label>
                <select class="custom-select" name="region_id">
                    <option value="">Выбрать регион</option>
                    @if ($params['regions'])
                        @foreach ($params['regions'] as $region)
                            <option value="{{ $region->id }}" @if ($ad_medium && $ad_medium->region_id == $region->id) selected @endif>{{ $region->title }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col col-sm-12 col-md-6 col-lg-6 form-group">
                <label class="custom-label">Название локации:</label>
                <input type="text" class="form-control" name="title" value="{{ ($ad_medium) ? $ad_medium->title : '' }}">
            </div>
            <div class="col col-sm-12 col-md-6 col-lg-6 form-group">
                <label class="custom-label">Тип локации:</label>
                <select class="custom-select" name="building_type_id">
                    <option value="">Выбрать тип локации</option>
                    @if ($params['building_types'])
                        @foreach ($params['building_types'] as $building_type)
                            <option value="{{ $building_type->id }}" @if ($ad_medium && $ad_medium->building_type_id == $building_type->id) selected @endif>{{ $building_type->title }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col col-sm-12 col-sm-12 col-md-6 col-lg-6 form-group">
                <label class="custom-label">Тип носителя:</label>
                <select class="custom-select" name="media_type_id">
                    <option value="">Выбрать тип носителя</option>
                    @if ($params['media_types'])
                        @foreach ($params['media_types'] as $media_type)
                            <option value="{{ $media_type->id }}" @if ($ad_medium && $ad_medium->media_type_id == $media_type->id) selected @endif>{{ $media_type->title }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col col-sm-12 col-md-6 col-lg-6 form-group">
                <label class="custom-label">Тип носителя (название):</label>
                <input type="text" class="form-control" name="media_type_orig_title" value="{{ ($ad_medium) ? $ad_medium->media_type_orig_title : '' }}">
            </div>
            <div class="col col-sm-12 col-md-6 col-lg-6 form-group">
                <label class="custom-label">Размер:</label>
                <input type="text" class="form-control" name="size" value="{{ ($ad_medium) ? $ad_medium->size : '' }}">
            </div>
            <div class="col col-sm-12 col-md-6 col-lg-6 form-group">
                <label class="custom-label with-radio">Ориентация:</label>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="orientation_a" name="orientation" class="custom-control-input" value="горизонтальный" @if ($ad_medium && $ad_medium->orientation == 'горизонтальный') checked @endif>
                    <label class="custom-control-label" for="orientation_a">горизонтальный</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="orientation_b" name="orientation" class="custom-control-input" value="вертикальный" @if ($ad_medium && $ad_medium->orientation == 'вертикальный') checked @endif>
                    <label class="custom-control-label" for="orientation_b">вертикальный</label>
                </div>
            </div>
            <div class="col col-sm-12 col-md-6 col-lg-6 form-group">
                <label class="custom-label">Охват аудитории:</label>
                <input type="number" class="form-control" name="network_coverage" value="{{ ($ad_medium && $ad_medium->network_coverage > 0) ? $ad_medium->network_coverage : 0 }}" min="0">
            </div>
            <div class="col col-sm-12 col-md-12 col-lg-12 form-group">
                <label class="custom-label">Адрес: <a href="#" id="js-find-address">найти</a></label>
                <textarea class="form-control" name="address">{{ ($ad_medium) ? $ad_medium->address : '' }}</textarea>
                <small class="form-text text-muted"><i class="fa fa-info-circle"></i> Для поиска адреса на карте должен быть выбран «Регион»</small>
            </div>
            <div class="col col-sm-12 col-md-12 col-lg-12 form-group">
                <div id="map" style="width: 100%; height: 400px"></div>
            </div>
            <div class="col col-sm-12 col-md-12 col-lg-12 form-group">
                <label class="custom-label">Изображения:</label>
                <input type="hidden" name="delete_files" value="[]">
                <input type="file" name="files" class="invisible" multiple accept=".jpg, .jpeg, .png, .tiff, .bmp">
                </br>
                <a href="#" class="btn btn-sm btn-outline-brand" id="js-add-file"><i class="fa fa-images"></i> Добавить</a>
                </br>
                <div class="images-list">
                    @if ($ad_medium && $ad_medium->files)
                        @foreach ($ad_medium->files as $file)
                            <div class="image-item">
                                <img src="{{ '/images/'.$file->name.'?w=350' }}">
                                <span data-name="{{ $file->name }}" data-id="{{ $file->id }}" data-path="{{ '/uploads/'.$file->name }}"><i class="fa fa-trash-alt"></i></span>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <hr/>
        <a href="#" class="btn btn-brand" id="js-btn-save">
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            <span class="text">Сохранить</span>
        </a>
        <a href="{{ url('/ad-medium/list') }}" class="btn btn-outline-brand back">Назад</a>
    </form>

    @include('ad_medium.inc._js_form')

@endsection
<script id="js-validation-errors" type="text/template">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul>
            <% _.each(items, function(error) { %>
                <li><%= error[0] %></li>
            <% }); %>
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</script>
<script id="js-image-item" type="text/template">
    <div class="image-item">
        <img src="<%= path %>">
        <span data-name="<%= name %>" data-id="" data-path=""><i class="fa fa-trash-alt"></i></span>
    </div>
</script>
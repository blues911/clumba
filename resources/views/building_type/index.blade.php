@extends('layout.main')

@section('title')

    Справочники &ndash; Типы локации

@endsection

@section('content')

    <h4 class="title" id="building-type-list-block">Справочники / Типы локации</h4>

    @include('layout.inc._flash_messages')

    <a href="{{ url('dictionary/building-type/create') }}" class="btn btn-sm btn-brand mb-4"><i class="fa fa-plus"></i> Добавить</a>

    @if ($building_types)

        <table class="table">
            <thead>
                <tr>
                    <th scope="col" class="w100">№</th>
                    <th scope="col">Название</th>
                    <th scope="col" class="w100"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($building_types as $building_type)
                    <tr>
                        <th scope="row">{{ $params['number']++ }}</th>
                        <td>{{ $building_type->title }}</td>
                        <td>
                            <a href="{{ url('dictionary/building-type/edit/' . $building_type->id) }}" class="edit-item"><i class="fas fa-edit"></i></a>
                            <a href="#" class="delete-item js-building-type-delete"><i class="fas fa-trash-alt"></i></a>
                            <form action="{{ url('dictionary/building-type/delete/' . $building_type->id) }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $building_types->links() }}

    @else

        <p>Нет данных</p>

    @endif

@endsection
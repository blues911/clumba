@extends('layout.main')

@section('title')

    {!! ($building_type) ? 'Справочники &ndash; Редактировать тип локации' : 'Справочники &ndash; Добавить тип локации' !!}

@endsection

@section('content')

    <h4 class="title" id="building-type-form-block">{{ ($building_type) ? 'Справочники / Редактировать тип локации' : 'Справочники / Добавить тип локации' }}</h4>

    @include('layout.inc._flash_messages')
    @include('layout.inc._validation_errors')

    <form id="building-type-form" class="edit-form" action="{{ ($building_type) ? url('dictionary/building-type/update/' . $building_type->id) : url('dictionary/building-type/store') }}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col col-sm-12 col-md-12 col-lg-12 form-group">
                <label class="custom-label">Название:</label>
                <input type="text" class="form-control" name="title" value="{{ ($building_type) ? $building_type->title : '' }}">
            </div>
        </div>
        <hr/>
        <button type="submit" class="btn btn-brand">Сохранить</button>
        <a href="{{ url('/dictionary/building-type/list') }}" class="btn btn-outline-brand back">Назад</a>
    </form>

@endsection
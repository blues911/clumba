@extends('layout.auth')

@section('title')

    Вход

@endsection

@section('content')

    <div id="login-content">
        <div class="logo">
            <img src="{{ asset('img/logo.svg') }}" />
        </div>
        <form action="{{ url('login') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="remember" value="on">
            <div class="form-group">
                <input type="text" name="login" class="form-control" placeholder="Логин">
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="Пароль">
            </div>
            @if (isset($errors) && $errors->any())
                <div class="alert alert-danger" role="alert">
                    Неверный логин или пароль
                </div>
            @endif
            <button type="submit" class="btn btn-brand">Войти</button>
        </form>
    </div>

@endsection
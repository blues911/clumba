@extends('layout.main')

@section('title')

    {!! ($region) ? 'Справочники &ndash; Редактировать регион' : 'Справочники &ndash; Добавить регион' !!}

@endsection

@section('content')

    <h4 class="title" id="region-form-block">{{ ($region) ? 'Справочники / Редактировать регион' : 'Справочники / Добавить регион' }}</h4>

    @include('layout.inc._flash_messages')
    @include('layout.inc._validation_errors')

    <form id="region-form" class="edit-form" action="{{ ($region) ? url('dictionary/region/update/'.$region->id) : url('dictionary/region/store') }}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col col-sm-12 col-md-12 col-lg-12 form-group">
                <label class="custom-label">Название:</label>
                <input type="text" class="form-control" name="title" value="{{ ($region) ? $region->title : '' }}">
            </div>
        </div>
        <hr/>
        <button type="submit" class="btn btn-brand">Сохранить</button>
        <a href="{{ url('/dictionary/region/list') }}" class="btn btn-outline-brand back">Назад</a>
    </form>

@endsection
@extends('layout.main')

@section('title')

    Справочники &ndash; Регионы

@endsection

@section('content')

    <h4 class="title" id="region-list-block">Справочники / Регионы</h4>

    @include('layout.inc._flash_messages')

    <a href="{{ url('dictionary/region/create') }}" class="btn btn-sm btn-brand mb-4"><i class="fa fa-plus"></i> Добавить</a>

    @if ($regions)

        <table class="table">
            <thead>
                <tr>
                    <th scope="col" class="w100">№</th>
                    <th scope="col">Название</th>
                    <th scope="col" class="w100"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($regions as $region)
                    <tr>
                        <th scope="row">{{ $params['number']++ }}</th>
                        <td>{{ $region->title }}</td>
                        <td>
                            <a href="{{ url('dictionary/region/edit/' . $region->id) }}" class="edit-item"><i class="fas fa-edit"></i></a>
                            <a href="#" class="delete-item js-region-delete"><i class="fas fa-trash-alt"></i></a>
                            <form action="{{ url('dictionary/region/delete/' . $region->id) }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $regions->links() }}

    @else

        <p>Нет данных</p>

    @endif

@endsection
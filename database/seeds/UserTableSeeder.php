<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'login' => 'user1',
                'password' => Hash::make('user1'),
                'type' => 'admin',
            ],
            [
                'login' => 'user2',
                'password' => Hash::make('user2'),
                'type' => 'manager',
            ],
        ];

        foreach ($items as $item)
        {
            DB::table('user')->insert($item);
        }
    }
}
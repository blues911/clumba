<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdMedium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_medium', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ext_id')->unique()->nullable();
            $table->bigInteger('region_id')->unsigned()->nullable();
            $table->bigInteger('building_type_id')->unsigned()->nullable();
            $table->bigInteger('media_type_id')->unsigned()->nullable();
            $table->string('media_type_orig_title')->nullable();
            $table->string('title');
            $table->string('address')->nullable();
            $table->decimal('map_lat', 11, 8)->nullable();
            $table->decimal('map_lon', 11, 8)->nullable();
            $table->string('size', 100)->nullable();
            $table->enum('orientation', ['вертикальный', 'горизонтальный']);
            $table->string('hash')->nullable();
        });

        Schema::table('ad_medium', function(Blueprint $table) {
            $table->foreign('region_id')
                ->references('id')
                ->on('region')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('building_type_id')
                ->references('id')
                ->on('building_type')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('media_type_id')
                ->references('id')
                ->on('media_type')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ad_medium', function(Blueprint $table) {
            $table->dropForeign('ad_medium_region_id_foreign');
            $table->dropForeign('ad_medium_building_type_id_foreign');
            $table->dropForeign('ad_medium_media_type_id_foreign');
        });

        Schema::dropIfExists('ad_medium');
    }
}

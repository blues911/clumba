<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdMediumImg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_medium_img', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ad_medium_id')->unsigned();
            $table->string('name');
            $table->string('orig_name');
            $table->string('mime_type');
            $table->string('size');
            $table->string('extension');
        });

        Schema::table('ad_medium_img', function(Blueprint $table) {
            $table->foreign('ad_medium_id')
                ->references('id')
                ->on('ad_medium')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ad_medium_img', function(Blueprint $table) {
            $table->dropForeign('ad_medium_img_ad_medium_id_foreign');
        });

        Schema::dropIfExists('ad_medium_img');
    }
}

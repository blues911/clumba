<?

$pdo = null;

$db_host = "h003301565.mysql";
$db_user = "h003301565_lk";
$db_pass = "Yhjs%aRt90s";
$db_name = "h003301565_lk";

try
{
    $pdo = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec("SET NAMES utf8");
}
catch(PDOException $e)
{
    echo "Error: " . $e->getMessage();
    die;
}

// Ajax
if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $region_id = $_POST['region_id'];
    $building_type_ids = (isset($_POST['building_type_ids'])) ? $_POST['building_type_ids'] : [];
    $media_type_ids = (isset($_POST['media_type_ids'])) ? $_POST['media_type_ids'] : [];

    $query = "SELECT
            ad_medium.*,
            building_type.title as building_type_title,
            building_type.map_icon as building_type_map_icon,
            media_type.title as media_type_title
        FROM ad_medium";
    $query .= " LEFT JOIN building_type ON building_type.id = ad_medium.building_type_id";
    $query .= " LEFT JOIN media_type ON media_type.id = ad_medium.media_type_id";

    if ($region_id > 0)
    {
        $query .= " WHERE ad_medium.region_id = " . $region_id;

        if (!empty($building_type_ids))
            $query .= " AND ad_medium.building_type_id IN (" . implode(', ', $building_type_ids) . ")";
        if (!empty($media_type_ids))
            $query .= " AND ad_medium.media_type_id IN (" . implode(', ', $media_type_ids) . ")";
    }
    else 
    {
        if (!empty($building_type_ids) && !empty($media_type_ids))
        {
            $query .= " WHERE ad_medium.building_type_id IN (" . implode(', ', $building_type_ids) . ")";
            $query .= " AND ad_medium.media_type_id IN (" . implode(', ', $media_type_ids) . ")";
        }
        else if (!empty($building_type_ids) && empty($media_type_ids))
        {
            $query .= " WHERE ad_medium.building_type_id IN (" . implode(', ', $building_type_ids) . ")";
        }
        else if (empty($building_type_ids) && !empty($media_type_ids))
        {
            $query .= " WHERE ad_medium.media_type_id IN (" . implode(', ', $media_type_ids) . ")";
        }
    }

    $sql = $pdo->prepare($query);
    $sql->execute();
    $items = $sql->fetchAll(PDO::FETCH_ASSOC);

    foreach ($items as $k => $item)
    {
        $sql = $pdo->prepare("SELECT * FROM ad_medium_img WHERE ad_medium_id = " . $item['id']);
        $sql->execute();
        $items[$k]['images'] = $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    echo json_encode($items);

    die;
}

$regions = [];
$building_types = [];
$media_types = [];

$sql = $pdo->prepare("SELECT region.*
                        FROM ad_medium
                        LEFT JOIN region
                            ON ad_medium.region_id = region.id
                        GROUP BY ad_medium.region_id
                        ORDER BY region.title ASC"
        );
$sql->execute();
$regions = $sql->fetchAll(PDO::FETCH_ASSOC);
array_unshift($regions, ['id' => 0, 'title' => 'Все регионы']);

$sql = $pdo->prepare("SELECT building_type.*
                        FROM ad_medium
                        LEFT JOIN building_type
                            ON ad_medium.building_type_id = building_type.id
                        GROUP BY ad_medium.building_type_id
                        ORDER BY building_type.title ASC"
        );
$sql->execute();
$building_types = $sql->fetchAll(PDO::FETCH_ASSOC);

$sql = $pdo->prepare("SELECT media_type.*
                        FROM ad_medium
                        LEFT JOIN media_type
                            ON ad_medium.media_type_id = media_type.id
                          GROUP BY ad_medium.media_type_id
                          ORDER BY media_type.title ASC"
        );
$sql->execute();
$media_types = $sql->fetchAll(PDO::FETCH_ASSOC);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetAdditionalCSS("/bitrix/templates/address/css/styles.css");
$APPLICATION->AddHeadScript("https://api-maps.yandex.ru/2.1/?apikey=5f54f981-e526-419b-9f94-536ea617cbf5&lang=ru_RU");
$APPLICATION->AddHeadScript("/bitrix/templates/address/js/jquery.nicescroll.min.js");
$APPLICATION->AddHeadScript("/bitrix/templates/address/js/masonry.pkgd.min.js");
$APPLICATION->AddHeadScript("/bitrix/templates/address/js/imagesloaded.pkgd.min.js");
$APPLICATION->AddHeadScript("/bitrix/templates/address/js/scripts.js");
$APPLICATION->SetTitle("Адресная программа");

?>

<div class="map-content container">
    <div class="map-filters">
        <div class="row">
            <div class="col-20 col-md-5 col-lg-5">
                <div class="region">
                    <div class="region-label">Регион:</div>
                    <div class="region-title">
                      <span>Москва</span>
                      <img src="/bitrix/templates/address/img/city_polygon.png"></div>
                    <ul class="region-list">
                        <? foreach ($regions as $k => $region) : ?>
                            <li data-id="<?= $region['id'] ?>" class="<?= ($region['id'] === 0) ? 'active' : '' ?>"><?= $region['title'] ?></li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="col-20 col-md-15 col-lg-15">
                <? if (!empty($building_types)) : ?>
                    <div class="row building-type">
                        <div class="col-20 col-sm-4 col-md-3 lg-3">
                            <strong>Тип локации:</strong>
                        </div>
                        <div class="col-20 col-sm-16 col-md-17 lg-17">
                            <? foreach ($building_types as $k => $building_type) : ?>
                                <label class="chx-container custom-checkbox">
                                    <?= $building_type['title'] ?>
                                    <input type="checkbox" class="building-type-checkbox" name="building_type_ids[]" value="<?= $building_type['id'] ?>">
                                    <span class="chx-checkmark"></span>
                                </label>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>
                <? if (!empty($media_types)) : ?>
                    <div class="row media-type">
                        <div class="col-20 col-sm-4 col-md-3 lg-3">
                            <strong>Тип носителя:</strong>
                        </div>
                        <div class="col-20 col-sm-16 col-md-17 lg-17">
                            <? foreach ($media_types as $k => $media_type) : ?>
                                <label class="chx-container custom-checkbox">
                                    <?= $media_type['title'] ?>
                                    <input type="checkbox" class="media-type-checkbox" id="media-type-<?= $k ?>" name="media_type_ids[]" value="<?= $media_type['id'] ?>">
                                    <span class="chx-checkmark"></span>
                                </label>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
    <div class="map-wrapper">
        <div id="map" style="width: 100%; height: 620px; margin-bottom: 20px;" data-content="[]"></div>
        <div id="placemark-info" data-content="">
            <div class="placemark-info-content">
                <img src="/bitrix/templates/address/img/modal_close.png" class="close">
                <div class="placemark-info-title"></div>
                <div class="placemark-info-address"></div>
                <div class="placemark-info-params">
                    <div class="placemark-info-params-media-type"><span class="title">Тип носителя:</span> <span class="text"></span></div>
                    <div class="placemark-info-params-size"><span class="title">Размеры:</span> <span class="text" title=""></span></div>
                    <div class="placemark-info-params-orientation"><span class="title">Ориентация:</span> <span class="text"></span></div>
                    <div class="placemark-info-params-images">
                        <div class="images-grid"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="placemark-images">
            <img src="/bitrix/templates/address/img/modal_close.png" class="close">
            <img src="/bitrix/templates/address/img/modal_prev.png" class="prev" data-id="">
            <img src="/bitrix/templates/address/img/modal_next.png" class="next" data-id="">
            <span class="back">&larr; назад</span>
            <div class="image-preview"></div>
            <span class="total"></span>
        </div>
    </div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>
ymaps.ready(init);

function init() {

    var currentZoom = 0;
    var $imagesGrid;
    var activeObjectId = null;
    var geoObjects = [];
    var map = new ymaps.Map("map", {
        center: [55.7558, 37.6173], // latitude, longitude
        controls: ['zoomControl', 'typeSelector'],
        zoom: 10
    });
    var objectManager = new ymaps.ObjectManager({
        clusterize: false,
        clusterIconLayout: ymaps.templateLayoutFactory.createClass('<div class="cluster-icon">{{ properties.geoObjects.length }}</div>'),
        clusterIconShape: {
            type: 'Rectangle',
            coordinates: [[-25, -25], [25, 25]]
        },
        clusterHideIconOnBalloonOpen: false,
        geoObjectHideIconOnBalloonOpen: false,
        clusterBalloonContentLayout: ymaps.templateLayoutFactory.createClass([
            '<ul class="cluster-placemarks-list">',
                '{% for geoObject in properties.geoObjects %}',
                    '<li data-id="{{ geoObject.data.id }}">{{ geoObject.data.address }} ({{geoObject.data.size}}, {{geoObject.data.media_type_title}})</li>',
                '{% endfor %}',
            '</ul>'
        ].join(''))
    });

    map.behaviors.disable('scrollZoom');

    map.events.add('boundschange', function (event) {

        currentZoom = event.get('newZoom');
        console.log(currentZoom);

        if (event.get('newZoom') !== event.get('oldZoom')) {

            $('#placemark-info').removeClass('active');
            setPlacemarkInfo({});
            resetPlacemarkIcon();

            if (currentZoom <= 8) {
                objectManager.options.set({clusterize: true});
            } else {
                objectManager.options.set({clusterize: false});
            }

            if (currentZoom <= 10) {
                objectManager.objects.each(function (object) {
                    var icon = (object.data.building_type_map_icon != null) ? object.data.building_type_map_icon : 'default.png';
                    if (object.data.title.toLowerCase().includes('world class')) {
                        icon = 'world_class.png';
                    }
                    if (icon.match(/^burger_king|world_class|business_center|shopping_center|fitness_center|supermarket(.*$)/)) {
                        icon = icon.replace(/.png/, '_sm.png');
                    }
                    objectManager.objects.setObjectOptions(object.id, {
                        iconImageHref: '/bitrix/templates/address/img/map_icons/' + icon,
                        iconImageSize: [10, 10],
                        iconImageOffset: [-5, -5]
                    });
                });
            } else if (currentZoom >= 11 && currentZoom <= 12) {
                objectManager.objects.each(function (object) {
                    var icon = (object.data.building_type_map_icon != null) ? object.data.building_type_map_icon : 'default.png';
                    if (object.data.title.toLowerCase().includes('world class')) {
                        icon = 'world_class.png';
                    }
                    objectManager.objects.setObjectOptions(object.id, {
                        iconImageHref: '/bitrix/templates/address/img/map_icons/' + icon,
                        iconImageSize: [32, 32],
                        iconImageOffset: [-16, -16]
                    });
                });
            } else if (currentZoom >= 13) {
                objectManager.objects.each(function (object) {
                    var icon = (object.data.building_type_map_icon != null) ? object.data.building_type_map_icon : 'default.png';
                    if (object.data.title.toLowerCase().includes('world class')) {
                        icon = 'world_class.png';
                    }
                    objectManager.objects.setObjectOptions(object.id, {
                        iconImageHref: '/bitrix/templates/address/img/map_icons/' + icon,
                        iconImageSize: [52, 52],
                        iconImageOffset: [-26, -26]
                    });
                });
            }
        }
    });

    $(document).ready(function() {

        // Установка хештега и id региона
        var hash = window.location.hash;
        var selectedId = (hash.search('#region=') == 0) ? hash.replace('#region=', '') : 0;

        if (!Number.isInteger(Number(selectedId))
            || $('.region-list li[data-id="' + Number(selectedId) + '"]').length == 0) {
            selectedId = 0;
        }

        $('.region-list li[data-id="' + Number(selectedId) + '"]').trigger('click');

        // Кастомный скролл
        $('.placemark-info-params-images').niceScroll({
            cursorcolor: '#888888',
            cursorborder: '1px solid #888888',
            background: '#e6e6e6',
            cursoropacitymin: 1,
            cursorwidth: '6px',
            cursorborderradius: '1px',
            horizrailenabled: false,
            autohidemode: 'leave'
        });
    });

    // Выбрать регион из списка
    $('.region-title').on('click', function(){
        $('.region-list').toggleClass('active');
        $('#placemark-info').removeClass('active');
        $('#placemark-images').removeClass('active');
        map.balloon.close();
    });

    $('.region-list li').on('click', function(){

        var regionId = $(this).data('id');
        var regionTitle = $(this).text();

        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $('.region-title span').text(regionTitle);
        $('.region-list').removeClass('active');
        window.location.hash = '#region=' + regionId;

        if (regionTitle == 'Все регионы') {
            regionTitle = 'Москва';
        }

        ymaps.geocode(regionTitle, {
            results: 1
        }).then(function (res) {
            var coords = res.geoObjects.get(0).geometry.getCoordinates();
            map.setCenter(coords);
            map.setZoom(10);
            initPlacemarks();
        });
    });

    // Фильтры
    $('.custom-checkbox input[type="checkbox"]').on('change', function() {
        initPlacemarks();
    });

    $(document).on('click', 'ul.cluster-placemarks-list li', function(){
        var id = $(this).data('id');
        var items = $('#map').data('content');

        map.balloon.close();

        for (var i = 0; i < items.length; i++) {
            if (items[i]['id'] == id) {
                setPlacemarkInfo(items[i]);
                break;
            }
        }
    });

    // Окно информации по точке
    $('#placemark-info .close').on('click', function() {
        $('#placemark-info').removeClass('active');
        setPlacemarkInfo({});
        resetPlacemarkIcon();
    });

    // Окно изображений по точке
    $(document).on('click', '#placemark-info .images-grid-item img', function(e) {
        e.preventDefault();
        var id = Number($(this).attr('data-id'));
        var data = JSON.parse($('#placemark-info').attr('data-content'));
        setPlacemarkImage(data, id);
        $('#placemark-images').addClass('active');
    });

    $('#placemark-images .back').on('click', function() {
        $('#placemark-images').removeClass('active');
        $('#placemark-images .image-preview').css({'background-image': ''});
        $('#placemark-images img.prev').attr('data-id', 0);
        $('#placemark-images img.next').attr('data-id', 0);
        $('#placemark-images span.total').text('');
    });

    $('#placemark-images .close').on('click', function() {
        $('#placemark-info').removeClass('active');
        setPlacemarkInfo({});
        resetPlacemarkIcon();
        $('#placemark-images').removeClass('active');
        $('#placemark-images .image-preview').css({'background-image': ''});
        $('#placemark-images img.prev').attr('data-id', 0);
        $('#placemark-images img.next').attr('data-id', 0);
        $('#placemark-images span.total').text('');
    });

    $(document).on('click', '#placemark-images img.prev', function() {
        var id = Number($(this).attr('data-id'));
        var data = JSON.parse($('#placemark-info').attr('data-content'));
        setPlacemarkImage(data, id);
    });

    $(document).on('click', '#placemark-images img.next', function() {
        var id = Number($(this).attr('data-id'));
        var data = JSON.parse($('#placemark-info').attr('data-content'));
        setPlacemarkImage(data, id);
    });

    function initPlacemarks() {

        var regionId =  $('.region-list li.active').data('id');
        var buildingTypeIds = [];
        var mediaTypeIds = [];

        $('.building-type-checkbox:checked').each(function(i) {
            buildingTypeIds[i] = $(this).val();
        });

        $('.media-type-checkbox:checked').each(function(i) {
            mediaTypeIds[i] = $(this).val();
        });

        $.ajax({
            url: '/address/',
            type: 'POST',
            data: {
                region_id: regionId,
                building_type_ids: buildingTypeIds,
                media_type_ids: mediaTypeIds
            },
            dataType: 'JSON',
            success: function (result) {

                $('#map').attr('data-content', JSON.stringify(result));
                $('#placemark-info').removeClass('active');
                $('#placemark-images').removeClass('active');
                setPlacemarkInfo({});

                map.geoObjects.removeAll();
                objectManager.removeAll();
                geoObjects = [];

                for (var i = 0; i < result.length; i++) {

                    var iconSettings = {
                        iconLayout: 'default#image',
                        iconImageHref: '',
                        iconImageSize: [],
                        iconImageOffset: []
                    };

                    var icon = (result[i]['building_type_map_icon'] != null) ? result[i]['building_type_map_icon'] : 'default.png';

                    if (result[i]['title'].toLowerCase().includes('world class')) {
                        icon = 'world_class.png';
                    }

                    if (currentZoom <= 10) {
                        if (icon.match(/^burger_king|world_class|business_center|shopping_center|fitness_center|supermarket(.*$)/)) {
                            icon = icon.replace(/.png/, '_sm.png');
                        }
                        iconSettings.iconImageSize = [10, 10];
                        iconSettings.iconImageOffset = [-5, -5];
                    } else if (currentZoom >= 11 && currentZoom <= 12) {
                        iconSettings.iconImageSize = [32, 32];
                        iconSettings.iconImageOffset = [-16, -16];
                    } else if (currentZoom >= 13) {
                        iconSettings.iconImageSize = [52, 52];
                        iconSettings.iconImageOffset = [-26, -26];
                    }

                    iconSettings.iconImageHref = '/bitrix/templates/address/img/map_icons/' + icon;

                    geoObjects.push({
                        type: 'Feature',
                        id: i,
                        data: result[i],
                        geometry: {
                            type: 'Point',
                            coordinates:[result[i]['map_lat'], result[i]['map_lon']]
                        },
                        options: iconSettings
                    });
                }

                objectManager.add(geoObjects);

                objectManager.objects.events.add('click', function (e) {
                    var objectId = e.get('objectId');
                    var obj = objectManager.objects.getById(objectId);

                    if (activeObjectId == objectId) {
                        return false;
                    }
            
                    if (activeObjectId != null && activeObjectId != objectId) {
                        resetPlacemarkIcon();
                    }
            
                    activeObjectId = objectId;

                    if (currentZoom <= 10) {
                        objectManager.objects.setObjectOptions(objectId, {
                            iconImageHref: '/bitrix/templates/address/img/map_icons/selected.png',
                            iconImageSize: [10, 12],
                            iconImageOffset: [-5, -6]
                        });
                    } else if (currentZoom >= 11 && currentZoom <= 12) {
                        objectManager.objects.setObjectOptions(objectId, {
                            iconImageHref: '/bitrix/templates/address/img/map_icons/selected.png',
                            iconImageSize: [24, 30],
                            iconImageOffset: [-12, -15]
                        });
                    } else if (currentZoom >= 13) {
                        objectManager.objects.setObjectOptions(objectId, {
                            iconImageHref: '/bitrix/templates/address/img/map_icons/selected.png',
                            iconImageSize: [40, 50],
                            iconImageOffset: [-20, -25]
                        });
                    }

                    map.setCenter(obj.geometry.coordinates);
                    setPlacemarkInfo(obj.data);

                    $('.region-list').removeClass('active');
                });

                map.geoObjects.add(objectManager);

                map.setBounds(map.geoObjects.getBounds(), {
                    checkZoomRange: true,
                });
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    function setPlacemarkInfo(obj) {

        if (typeof $imagesGrid == 'object') {
            $imagesGrid.masonry('destroy');
            $imagesGrid = undefined;
        }

        $('#placemark-info').removeClass('sm');

        if (Object.keys(obj).length === 0) {

            $('#placemark-info').attr('data-content', '');
            $('#placemark-info .placemark-info-title').text('');
            $('#placemark-info .placemark-info-address').text('');
            $('#placemark-info .placemark-info-params-media-type span.text').text('');
            $('#placemark-info .placemark-info-params-media-type').removeClass('hidden');
            $('#placemark-info .placemark-info-params-size span.text').text('');
            $('#placemark-info .placemark-info-params-size span.text').attr('title', '');
            $('#placemark-info .placemark-info-params-size').removeClass('hidden');
            $('#placemark-info .placemark-info-params-orientation span.text').text('');
            $('#placemark-info .placemark-info-params-orientation').removeClass('hidden');
            $('#placemark-info .placemark-info-params-images .images-grid').html('');
        } else {

            $('#placemark-info').attr('data-content', JSON.stringify(obj));
            $('#placemark-info').addClass('active');
            $('#placemark-info .placemark-info-title').text(obj.title);
            $('#placemark-info .placemark-info-address').text(obj.address);

            if (obj.media_type_title) {
                $('#placemark-info .placemark-info-params-media-type span.text').text(obj.media_type_orig_title);
                $('#placemark-info .placemark-info-params-media-type').removeClass('hidden');
            } else {
                $('#placemark-info .placemark-info-params-media-type span.text').text('');
                $('#placemark-info .placemark-info-params-media-type').addClass('hidden');
            }

            if (obj.size) {
                $('#placemark-info .placemark-info-params-size span.text').attr('title', obj.size);
                $('#placemark-info .placemark-info-params-size span.text').text(obj.size);
                $('#placemark-info .placemark-info-params-size').removeClass('hidden');
            } else {
                $('#placemark-info .placemark-info-params-size span.text').attr('title', '');
                $('#placemark-info .placemark-info-params-size span.text').text('');
                $('#placemark-info .placemark-info-params-size').addClass('hidden');
            }

            if (obj.orientation) {
                $('#placemark-info .placemark-info-params-orientation span.text').text(obj.orientation);
                $('#placemark-info .placemark-info-params-orientation').removeClass('hidden');
            } else {
                $('#placemark-info .placemark-info-params-orientation span.text').text('');
                $('#placemark-info .placemark-info-params-orientation').addClass('hidden');
            }

            $('#placemark-info .placemark-info-params-images .images-grid').html('');
            $(document).find('.placemark-info-params-images').getNiceScroll().resize();

            if (obj.images.length > 0) {
                if (obj.images.length == 1) {
                    $('#placemark-info .placemark-info-params-images .images-grid').append([
                        '<div class="images-grid-item single">',
                            '<img src="https://lk.clumba.ru/images/'+obj.images[0].name+'?w=400" data-id="'+obj.images[0].id+'">',
                        '</div>',
                    ].join(''));
                } else {
                    for (var i = 0; i < obj.images.length; i++) {
                        $('#placemark-info .placemark-info-params-images .images-grid').append([
                            '<div class="images-grid-item">',
                                '<img src="https://lk.clumba.ru/images/'+obj.images[i].name+'?w=400" data-id="'+obj.images[i].id+'">',
                            '</div>',
                        ].join(''));
                    }

                    $imagesGrid = $('.images-grid').masonry({
                        itemSelector: '.images-grid-item',
                        columnWidth: 195,
                        gutter: 10
                    });

                    $imagesGrid.imagesLoaded().progress( function() {
                        $imagesGrid.masonry('layout');
                    });
                }
            } else {
                $('#placemark-info').addClass('sm');
            }

            setTimeout(function(){
                $(document).find('.placemark-info-params-images').getNiceScroll().resize();
                $(document).find('.nicescroll-rails').css({'width': '6px'});
            }, 500);
        }
    }

    function setPlacemarkImage(data, id) {

        $('#placemark-images img.prev').removeClass('hidden');
        $('#placemark-images img.next').removeClass('hidden');
        $('#placemark-images span.total').removeClass('hidden');

        if (id != 0) {

            for (var i = 0; i < data.images.length; i++) {

                if (data.images[i]['id'] == id) {
                    var indx = i;
                    var prevId = (data.images[indx-1] != undefined) ? data.images[indx-1]['id'] : 0;
                    var nextId = (data.images[indx+1] != undefined) ? data.images[indx+1]['id'] : 0;
                    $('#placemark-images .image-preview').css({'background-image': 'url(https://lk.clumba.ru/uploads/' + data.images[i]['name'] + ')'});
                    $('#placemark-images img.prev').attr('data-id', prevId);
                    $('#placemark-images img.next').attr('data-id', nextId);
                    $('#placemark-images span.total').text((indx+1)+'/'+data.images.length);
                    break;
                }
            }

            if (data.images.length == 1) {

                $('#placemark-images img.prev').addClass('hidden');
                $('#placemark-images img.next').addClass('hidden');
                $('#placemark-images span.total').addClass('hidden');
            }
        }
    }

    function resetPlacemarkIcon() {

        if (activeObjectId != null) {

            var iconSettings = {
                iconLayout: 'default#image',
                iconImageHref: '',
                iconImageSize: [],
                iconImageOffset: []
            };
            var oldObj = objectManager.objects.getById(activeObjectId);
            var icon = (oldObj.data.building_type_map_icon != null) ? oldObj.data.building_type_map_icon : 'default.png';

            if (oldObj.data.title.toLowerCase().includes('world class')) {
                icon = 'world_class.png';
            }

            if (currentZoom <= 10) {
                if (icon.match(/^burger_king|world_class|business_center|shopping_center|fitness_center|supermarket(.*$)/)) {
                    icon = icon.replace(/.png/, '_sm.png');
                }
                iconSettings.iconImageSize = [10, 10];
                iconSettings.iconImageOffset = [-5, -5];
            } else if (currentZoom >= 11 && currentZoom <= 12) {
                iconSettings.iconImageSize = [32, 32];
                iconSettings.iconImageOffset = [-16, -16];
            } else if (currentZoom >= 13) {
                iconSettings.iconImageSize = [52, 52];
                iconSettings.iconImageOffset = [-26, -26];
            }

            iconSettings.iconImageHref = '/bitrix/templates/address/img/map_icons/' + icon;

            objectManager.objects.setObjectOptions(activeObjectId, iconSettings);
            activeObjectId = null;
        }
    }
}
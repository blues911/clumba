<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BuildingType;
use App\Models\MediaType;

class AdMedium extends Model
{
    /**
     * Базовые поля даты.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации.
     * 
     * @var array
     */
    public static $rules = [
        'region_id' => 'required',
        'title' => 'required',
        'building_type_id' => 'required',
        'media_type_id' => 'required',
        'size' => 'required',
        'orientation' => 'required',
        'address' => 'required',
        'files.*' => 'max:10000|mimes:jpg,jpeg,png,tiff,bmp',
        'map_lon' => 'map_coordinates'
    ];

    /**
     * Сообщения для правил валидации.
     * 
     * @var array
     */
    public static $rulesMessages = [
        'region_id.required' => 'Поле «Регион» обязательно для заполнения.',
        'title.required' => 'Поле «Название локации» обязательно для заполнения.',
        'building_type_id.required' => 'Поле «Тип локации» обязательно для заполнения.',
        'media_type_id.required' => 'Поле «Тип носителя» обязательно для заполнения.',
        'size.required' =>  'Поле «Размер» обязательно для заполнения.',
        'orientation.required' => 'Поле «Ориентация» обязательно для заполнения.',
        'address.required' => 'Поле «Адрес» обязательно для заполнения.',
        'files.*.mimes' => 'Формат файла должен быть: jpg, jpeg, png, tiff, bmp.',
        'files.*.max' => 'Максимальный размер файла 10Mb.'
    ];

    /**
     * Название таблицы.
     * 
     * @var string
     */
    protected $table = 'ad_medium';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Изображения.
     * 
     * @return \App\Models\AdMediumImg
     */
    public function files()
    {
        return $this->hasMany('App\Models\AdMediumImg', 'ad_medium_id', 'id');
    }

    /**
     * Регион.
     * 
     * @return \App\Models\Region
     */
    public function region()
    {
        return $this->hasOne('App\Models\Region', 'id', 'region_id');
    }

    /**
     * Тип локации.
     * 
     * @return \App\Models\BuildingType
     */
    public function buildingType()
    {
        return $this->hasOne('App\Models\BuildingType', 'id', 'building_type_id');
    }

    /**
     * Тип носителя.
     * 
     * @return App\Models\MediaType
     */
    public function mediaType()
    {
        return $this->hasOne('App\Models\MediaType', 'id', 'media_type_id');
    }

    /**
     * Список рекламных носителей.
     * 
     * @param array $data
     * @param int $limit
     * @return \App\Models\AdMedium
     */
    public static function allForTable($data, $limit)
    {
        $query = self::with(['region', 'buildingType', 'mediaType']);

        if ($data['ext_id'] != '')
            $query->where('ext_id', $data['ext_id']);

        if ($data['region_id'] != '')
            $query->where('region_id', $data['region_id']);

        if ($data['building_type_id'] != '')
            $query->where('building_type_id', $data['building_type_id']);

        if ($data['media_type_id'] != '')
            $query->where('media_type_id', $data['media_type_id']);

        return $query->paginate($limit);
    }

    /**
     * Расчет точек с координатами.
     * 
     * @param array $data
     * @param array|null $sharedIds
     * @return array
     */
    public static function calcMapPoints($data, $sharedIds = null)
    {
        $result = [];

        $query = self::with(['region', 'buildingType', 'mediaType', 'files'])
            ->where('region_id', $data['region_id']);

        if (!empty($data['filters']['building_type_ids']))
            $query->whereIn('building_type_id', $data['filters']['building_type_ids']);

        if (!empty($data['filters']['media_type_ids']))
            $query->whereIn('media_type_id', $data['filters']['media_type_ids']);

        // Выборка только для публичной карты
        // TODO: поправить когда будет время!
        if (!empty($sharedIds))
        {
            $sharedIds = (count($sharedIds) == 0) ? [] : $sharedIds;
            $query->whereIn('id', $sharedIds);
        }

        $items = $query->get();

        if ($data['search_type'] == 'radius')
        {
            foreach ($data['addresses'] as $k => $values)
            {
                $result[$k]['address'] = $data['addresses'][$k];
                $result[$k]['points'] = [];
            }
    
            if (!empty($items))
            {
                foreach ($result as $k => $values)
                {
                    foreach ($items as $item)
                    {
                        $distance = calc_map_points_distance(
                              $result[$k]['address']['coords'][0],
                              $result[$k]['address']['coords'][1],
                              $item['map_lat'],
                              $item['map_lon']
                        );

                        if ($distance <= $data['filters']['radius'])
                        {
                            $item['distance'] = ceil($distance);
                            $result[$k]['points'][] = $item;
                        }
                    }
                }
            }
        }
        else
        {
            foreach ($data['filters']['polygon_coords'] as $k => $values)
            {
                $result[$k]['polygon_coords'] = $data['filters']['polygon_coords'][$k];
                $result[$k]['points'] = [];

                if ($k == 0)
                {
                    $result[$k]['addresses'] = !empty($data['addresses']) ? $data['addresses'] : [];
                }
            }

            if (!empty($items))
            {
                foreach ($result as $k => $values)
                {
                    foreach ($items as $item)
                    {
                        if (calc_map_point_in_polygon([$item['map_lat'], $item['map_lon']], $result[$k]['polygon_coords']))
                        {
                            $result[$k]['points'][] = $item;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Расчет точек с координатами + сбор их id.
     * 
     * @param array $data
     * @param array|null $sharedIds
     * @return array
     */
    public static function calcMapPointsIds($data, $sharedIds = null)
    {
        $ids = [];
        $result = [];

        $query = self::with(['region', 'buildingType', 'mediaType', 'files'])
            ->where('region_id', $data['region_id']);

        if (!empty($data['filters']['building_type_ids']))
            $query->whereIn('building_type_id', $data['filters']['building_type_ids']);

        if (!empty($data['filters']['media_type_ids']))
            $query->whereIn('media_type_id', $data['filters']['media_type_ids']);

        // Выборка только для публичной карты
        // TODO: поправить когда будет время!
        if (!empty($sharedIds))
        {
            $sharedIds = (count($sharedIds) == 0) ? [] : $sharedIds;
            $query->whereIn('id', $sharedIds);
        }

        $items = $query->get();

        if ($data['search_type'] == 'radius')
        {
            foreach ($data['addresses'] as $k => $values)
            {
                $result[$k]['address'] = $data['addresses'][$k];
                $result[$k]['points'] = [];
            }

            if (!empty($items))
            {
                foreach ($result as $k => $values)
                {
                    foreach ($items as $item)
                    {
                        $distance = calc_map_points_distance(
                            $result[$k]['address']['coords'][0],
                            $result[$k]['address']['coords'][1],
                            $item['map_lat'],
                            $item['map_lon']
                        );

                        if ($distance <= $data['filters']['radius'])
                        {
                            $ids[] = $item->id;
                        }
                    }
                }
            }
        }
        else
        {
            foreach ($data['filters']['polygon_coords'] as $k => $values)
            {
                $result[$k]['polygon_coords'] = $data['filters']['polygon_coords'][$k];
                $result[$k]['points'] = [];

                if ($k == 0)
                {
                    $result[$k]['addresses'] = !empty($data['addresses']) ? $data['addresses'] : [];
                }
            }

            if (!empty($items))
            {
                foreach ($result as $k => $values)
                {
                    foreach ($items as $item)
                    {
                        if (calc_map_point_in_polygon([$item['map_lat'], $item['map_lon']], $result[$k]['polygon_coords']))
                        {
                            $ids[] = $item->id;
                        }
                    }
                }
            }
        }

        return $ids;
    }

    /**
     * Расчет покрытия (сеть носителей).
     * 
     * @param array $data
     * @return array
     */
    public static function calcNetworkCoverage($data)
    {
        $result = '';
        $networkCoverage = 0;

        $mediaTypes = [];
        $mediaTypeList = MediaType::all();
        foreach ($mediaTypeList as $mediaTypeItem)
        {
            $mediaTypes[$mediaTypeItem->id]['title'] = $mediaTypeItem->title;
            $mediaTypes[$mediaTypeItem->id]['total'] = 0;
        }

        $buildingTypes = [];
        $buildingTypeList = BuildingType::all();
        foreach ($buildingTypeList as $buildingTypeItem)
        {
            $buildingTypes[$buildingTypeItem->id]['title'] = $buildingTypeItem->title;
            $buildingTypes[$buildingTypeItem->id]['media_types'] = $mediaTypes;
        }

        foreach ($data as $items)
        {
            foreach ($items['points'] as $point)
            {
                $buildingTypes[$point['building_type_id']]['media_types'][$point['media_type_id']]['total'] += 1;
                $networkCoverage += $point['network_coverage'];
            }
        }

        foreach ($buildingTypes as $buildingType)
        {
            $title = $buildingType['title'];
            foreach ($buildingType['media_types'] as $mediaType)
            {
                if ($mediaType['total'] > 0)
                {
                    $result .= $mediaType['total'] . ' ' . $mediaType['title'] . ' в ' . $title . ', ';
                }
            }
        }

        $result = rtrim(trim($result), ',');

        if ($result != '')
        {
            $result .= '. Охват сети носителей в месяц — ' . snumb_format($networkCoverage) . ' чел.';
            $result = 'Найдено ' . $result;

            return [
                'title' => $result,
                'status' => 1
            ];
        }
        else
        {
            $result .= 'Ничего не найдено';

            return [
                'title' => $result,
                'status' => 0
            ];
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MediaType extends Model
{
    /**
     * Базовые поля даты.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации.
     * 
     * @var array
     */
    public static $rules = [
        'title' => 'required',
    ];

    /**
     * Сообщения для правил валидации.
     * 
     * @var array
     */
    public static $rulesMessages = [
        'title.required' => 'Поле «Название» обязательно для заполнения.',
    ];

    /**
     * Название таблицы.
     * 
     * @var string
     */
    protected $table = 'media_type';

    /**
     * Первичный ключ.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Список.
     * 
     * @return \App\Models\MediaType
     */
    public static function getForList()
    {
        return self::orderBy('title', 'asc')->get();
    }

    /**
     * Список для карты.
     * 
     * @param array $adMediumIds
     * @return \App\Models\MediaType
     */
    public static function getForMap($adMediumIds)
    {
        return DB::table('media_type')
            ->select('media_type.*')
            ->leftJoin('ad_medium', 'media_type.id', '=', 'ad_medium.media_type_id')
            ->whereIn('ad_medium.id', $adMediumIds)
            ->groupBy('ad_medium.media_type_id')
            ->orderBy('media_type.title')
            ->get();
    }
}
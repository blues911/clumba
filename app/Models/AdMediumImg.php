<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdMediumImg extends Model
{
    /**
     * Базовые поля даты.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Название таблицы.
     * 
     * @var string
     */
    protected $table = 'ad_medium_img';

    /**
     * Первичный ключ.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Поля для заполнения.
     * 
     * @var array
     */
    protected $fillable = [
        'name',
        'orig_name',
        'mime_type',
        'size',
        'extension'
    ];
}

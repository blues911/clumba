<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * Базовые поля даты.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации.
     * 
     * @var array
     */
    public static $rules = [
        'title' => 'required',
    ];

    /**
     * Сообщения для правил валидации.
     * 
     * @var array
     */
    public static $rulesMessages = [
        'title.required' => 'Поле «Название» обязательно для заполнения.',
    ];

    /**
     * Название таблицы.
     * 
     * @var string
     */
    protected $table = 'region';

    /**
     * Первичный ключ.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Список.
     * 
     * @return \App\Models\Region
     */
    public static function getForList()
    {
        return self::orderBy('title', 'asc')->get();
    }
}
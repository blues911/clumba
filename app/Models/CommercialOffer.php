<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommercialOffer extends Model
{
    /**
     * Базовые поля даты.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Название таблицы.
     * 
     * @var string
     */
    protected $table = 'commercial_offer';

    /**
     * Первичный ключ.
     * 
     * @var string
     */
    protected $primaryKey = 'uid';
}
<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class BuildingType extends Model
{
    /**
     * Базовые поля даты.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации.
     * 
     * @var array
     */
    public static $rules = [
        'title' => 'required',
    ];

    /**
     * Сообщения для правил валидации.
     * 
     * @var array
     */
    public static $rulesMessages = [
        'title.required' => 'Поле «Название» обязательно для заполнения.',
    ];

    /**
     * Название таблицы.
     * 
     * @var string
     */
    protected $table = 'building_type';

    /**
     * Первичный ключ.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Список.
     * 
     * @return \App\Models\BuildingType
     */
    public static function getForList()
    {
        return self::orderBy('title', 'asc')->get();
    }

    /**
     * Список для карты.
     * 
     * @param array $adMediumIds
     * @return \App\Models\BuildingType
     */
    public static function getForMap($adMediumIds)
    {
        return DB::table('building_type')
            ->select('building_type.*')
            ->leftJoin('ad_medium', 'building_type.id', '=', 'ad_medium.building_type_id')
            ->whereIn('ad_medium.id', $adMediumIds)
            ->groupBy('ad_medium.building_type_id')
            ->orderBy('building_type.title')
            ->get();
    }
}
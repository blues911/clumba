<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Базовые поля даты.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Название таблицы.
     * 
     * @var string
     */
    protected $table = 'user';

    /**
     * Первичный ключ.
     * 
     * @var string
     */
    protected $primaryKey = 'id';
}

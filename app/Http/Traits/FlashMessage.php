<?php

namespace App\Http\Traits;

use Session;

/**
 * Generates quick temp messages based on Bootstrap Alerts.
 * https://getbootstrap.com/docs/4.4/components/alerts/
 */
trait FlashMessage
{
    /**
     * Available message types.
     * @var array
     */
    private $types = array('brand', 'primary', 'secondary', 'light', 'success', 'info', 'warning', 'danger');

    /**
     * Contains created messages.
     * @var array
     */
    private $messages = array();

    /**
     * Key for a session.
     * @var string
     */
    private $sessionKey = '_flash_messages_';

    /**
     * Creates a message.
     * @param string $type
     * @param string $message
     */ 
    public function add($type, $message)
    {
        if (in_array($type, $this->types)) {
            $this->messages[$type][] = $message;
            Session::flash($this->sessionKey, $this->messages);
        }
    }

    /**
     * Shortcuts per each type of messages.
     * @param string $name
     * @param array $arguments
     */
    public function __call($name, $arguments)
    {
        $name = strtolower(str_replace('flash', '', $name));

        if (in_array($name, $this->types)) {
            $message = array_shift($arguments);
            return $this->add($name, $message);
        }
    }
}
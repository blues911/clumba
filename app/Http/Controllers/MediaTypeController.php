<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\MediaType;
use App\Http\Traits\FlashMessage;

class MediaTypeController extends Controller
{
    use FlashMessage;

    /**
     * Кол-во типов носителя в списке.
     * 
     * @var int
     */
    protected $pageLimit = 30;

    /**
     * Список типов носителя.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params['number'] = ($request->query('page') && $request->query('page') > 1) ? $request->query('page') * $this->pageLimit : 1;
        
        $mediaTypes = MediaType::paginate($this->pageLimit);

        return view('media_type/index', [
            'params' => $params,
            'media_types' => $mediaTypes
        ]);
    }

    /**
     * Форма создания типа носителя.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('media_type/form', [
            'media_type' => false
        ]);
    }

    /**
     * Создание типа носителя.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            MediaType::$rules,
            MediaType::$rulesMessages
        )->validate();

        $mediaType = new MediaType;
        $mediaType->title = $request->input('title');
        $mediaType->save();

        $this->flashBrand('Тип носителя добавлен');

        return redirect('dictionary/media-type/list');
    }

    /**
     * Форма редактирования типа носителя.
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mediaType = MediaType::findOrFail($id);

        return view('media_type/form', [
            'media_type' => $mediaType
        ]);
    }

    /**
     * Обновление типа носителя.
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        Validator::make(
            $request->all(),
            MediaType::$rules,
            MediaType::$rulesMessages
        )->validate();

        $mediaType = MediaType::findOrFail($id);
        $mediaType->title = $request->input('title');
        $mediaType->aliases = $request->input('aliases') ?: null;

        $mediaType->save();

        $this->flashBrand('Тип носителя обновлен');

        return redirect('dictionary/media-type/list');
    }

    /**
     * Удаление типа носителя.
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function delete(Request $request, $id)
    {
        $mediaType = MediaType::findOrFail($id);
        $mediaType->delete();

        $this->flashBrand('Тип носителя удален');

        return redirect('dictionary/media-type/list');
    }
}
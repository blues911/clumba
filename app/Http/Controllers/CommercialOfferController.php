<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Models\AdMedium;
use App\Models\Region;
use App\Models\BuildingType;
use App\Models\MediaType;
use App\Models\CommercialOffer;
use App\Mail\FeedbackMail;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory as SpreadsheetIOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border as SpreadsheetStyleBorder;
use PhpOffice\PhpSpreadsheet\Style\Alignment as SpreadsheetStyleAlignment;

class CommercialOfferController extends Controller
{
    /**
     * Карта КП (коммерческие предложения).
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd(session('excel_export'));
        $isUser = auth()->check();
        $requestUri = $request->getRequestUri();

        // Редирект пользователя на его url
        if ($isUser && $requestUri == '/shared/commercial-offer')
            return redirect('commercial-offer/list');

        $request->session()->forget('excel_export');

        $params = [];
        $params['regions'] = Region::getForList();
        $params['building_types'] = BuildingType::getForList();
        $params['media_types'] = MediaType::getForList();

        return view('commercial_offer/index', [
            'params' => $params
        ]);
    }

    /**
     * Экспорт даных из сессии в excel файл.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \PhpOffice\PhpSpreadsheet\IOFactory || 404
     */
    public function excelExport(Request $request)
    {
        $filename = ($request->query('name')) ? $request->query('name') : 'clumba_' . date('YmdHis');
        $sessionData = $request->session()->get('excel_export');

        if (!$sessionData) abort(404);

        if ($sessionData['search_type'] == 'radius')
            $this->initExcelByRadius($sessionData, $filename);
        else
            $this->initExcelByPolygon($sessionData, $filename);
    }

    /**
     * Экспорт расчетов по радиусу.
     * 
     * @param array $data
     * @param string $filename
     */
    protected function initExcelByRadius($data, $filename)
    {
        $rows = [];
        $total = 3;

        // Колонки для мержа
        $mergeCols = [];
        $mergeCols[] = 'A1:I1';
        $mergeCols[] = 'A2:I2';

        // Границы для смерженных колонок
        // $mergeColsBorder = [];
        // $mergeColsBorder[] = 'A1:I1';

        // Границы лево, право, низ для отдельных наборов ячеек
        $leftColsBorder = [];
        $rightColsBorder = [];
        $topColsBorder = [];
        $bottomColsBorder = [];

        foreach ($data['items'] as $item)
        {
            $topColsBorder[] = 'A'.$total;
            // $merge = 'A'.$total;
            // $mergeColsBorder[] = 'A'.$total;

            if (count($item['points']) > 0)
            {
                $i = 0;
                foreach ($item['points'] as $point)
                {
                    if ($i == 0)
                    {
                        $rows[] = [
                            $item['address']['name'],
                            $point->buildingType->title,
                            $point['title'],
                            $point['address'],
                            $point['distance'] . '~ м',
                            ($point->mediaType) ? $point->mediaType->title : '',
                            $point['size'],
                            $point['orientation'],
                            $point['ext_id']
                        ];
                    }
                    else
                    {
                        $rows[] = [
                            '',
                            $point->buildingType->title,
                            $point['title'],
                            $point['address'],
                            $point['distance'] . '~ м',
                            ($point->mediaType) ? $point->mediaType->title : '',
                            $point['size'],
                            $point['orientation'],
                            $point['ext_id']
                        ];
                    }

                    $i++;
                }
            }
            else
            {
                $rows[] = [
                    $item['address']['name'],
                    'Рекламные носители в радиусе ' . $data['filters']['radius'] . 'м не найдены',
                    ''
                ];

                // $mergeCols[] = 'B'.$total.':I'.$total;
            }

            $total += (count($item['points']) > 0) ? count($item['points']) - 1 : 0;

            // $merge .= ':A' . $total;
            // $mergeCols[] = $merge;

            if (count($item['points']) > 0)
            {
                $rest = $total;
                $steps = count($item['points']);
                foreach ($item['points'] as $point)
                {
                    $bottomColsBorder[] = 'B' . $rest . ':I' . $rest;

                    $rightColsBorder[] = 'B' . $rest;
                    $rightColsBorder[] = 'C' . $rest;
                    $rightColsBorder[] = 'D' . $rest;
                    $rightColsBorder[] = 'E' . $rest;
                    $rightColsBorder[] = 'F' . $rest;
                    $rightColsBorder[] = 'G' . $rest;
                    $rightColsBorder[] = 'H' . $rest;
                    $rightColsBorder[] = 'I' . $rest;

                    $rest = $rest - 1;
                }
            }
            else 
            {
                $bottomColsBorder[] = 'B' . $total . ':I' . $total;
            }

            $total++;
        }

        $leftColsBorder[] = 'B3:B' . (($total > 2) ? $total - 1 : $total);
        $rightColsBorder[] = 'I1:I' . (($total > 2) ? $total - 1 : $total);

        $bottomColsBorder[] = 'A1:I1';
        $bottomColsBorder[] = 'A2:I2';
        $bottomColsBorder[] = 'A' . (($total > 2) ? $total - 1 : $total) . ':I' . (($total > 2) ? $total - 1 : $total);

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('CLUMBA');
        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getActiveSheet()->setCellValue('A1', $data['region']['title']);
        $spreadsheet->getActiveSheet()->setCellValue('A2', $data['items_info']['title']);
        $spreadsheet->getActiveSheet()->fromArray($rows, null, 'A3');

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);

        // Мерж колонок отключен

        // foreach ($mergeCols as $mergeCol)
        // {
        //     $spreadsheet->getActiveSheet()->mergeCells($mergeCol);
        //     $spreadsheet->getActiveSheet()->getStyle($mergeCol)->getAlignment()->setVertical(SpreadsheetStyleAlignment::VERTICAL_TOP);
        //     // $spreadsheet->getActiveSheet()->getStyle($mergeCol)->getFont()->setBold(true);
        // }

        // foreach ($mergeColsBorder as $mergeColBorder)
        // {
        //     $spreadsheet->getActiveSheet()->getStyle($mergeColBorder)->applyFromArray([
        //         'borders' => [
        //             'left' => [
        //                 'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
        //             ],
        //             'right' => [
        //                 'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
        //             ],
        //             'top' => [
        //                 'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
        //             ],
        //             'bottom' => [
        //                 'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
        //             ],
        //         ],
        //     ]);
        // }

        foreach ($leftColsBorder as $leftColBorder)
        {
            $spreadsheet->getActiveSheet()->getStyle($leftColBorder)->applyFromArray([
                'borders' => [
                    'left' => [
                        'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
                    ],
                ],
            ]);
        }

        foreach ($rightColsBorder as $rightColBorder)
        {
            $spreadsheet->getActiveSheet()->getStyle($rightColBorder)->applyFromArray([
                'borders' => [
                    'right' => [
                        'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
                    ],
                ],
            ]);
        }

        foreach ($topColsBorder as $topColBorder)
        {
            $spreadsheet->getActiveSheet()->getStyle($topColBorder)->applyFromArray([
                'borders' => [
                    'top' => [
                        'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
                    ],
                ],
            ]);
        }

        foreach ($bottomColsBorder as $bottomColBorder)
        {
            $spreadsheet->getActiveSheet()->getStyle($bottomColBorder)->applyFromArray([
                'borders' => [
                    'bottom' => [
                        'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
                    ],
                ],
            ]);
        }

        $writer = SpreadsheetIOFactory::createWriter($spreadsheet, 'Xls');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');

        $writer->save("php://output");
        exit;
    }

    /**
     * Экспорт расчетов произвольной области.
     * 
     * @param array $data
     * @param string $filename
     */
    protected function initExcelByPolygon($data, $filename)
    {
        $rows = [];
        $total = 3;
        $hasAddresses = (bool)!empty($data['addresses']);

        // Границы лево, право, низ для отдельных наборов ячеек
        $leftColsBorder = [];
        $rightColsBorder = [];
        $topColsBorder = [];
        $bottomColsBorder = [];

        if ($data['items_info']['status'] == 1)
        {
            foreach ($data['items'] as $item)
            {
                if (count($item['points']))
                {
                    $i = 0;
                    foreach ($item['points'] as $point)
                    {
                        $r = [
                            $point->buildingType->title,
                            $point['title'],
                            $point['address'],
                            ($point->mediaType) ? $point->mediaType->title : '',
                            $point['size'],
                            $point['orientation'],
                            $point['ext_id']
                        ];

                        if ($hasAddresses) array_unshift($r, '');
                        $rows[] = $r;
    
                        $i++;
                    }
                }
            }

            if ($hasAddresses)
            {
                for ($i = 0; $i < count($data['addresses']); $i++)
                {
                    $rows[$i][0] = $data['addresses'][$i]['name'];

                    if ($i > 0 && count($rows[$i]) == 1)
                        $rows[$i] = [$rows[$i][0], '', '', '', '', '', '', ''];
                }

                $n = count($rows) + 2;

                $leftColsBorder = ['B3:B' . $n];
  
                $rightColsBorder = [
                    'A3:A' . $n,
                    'B3:B' . $n,
                    'C3:C' . $n,
                    'D3:D' . $n,
                    'E3:E' . $n,
                    'F3:F' . $n,
                    'G3:G' . $n,
                    'H1:H' . $n
                ];

                $bottomColsBorder = [
                    'A1:H1',
                    'A2:H2',
                    'A' . $n . ':H' . $n
                ];
            }
            else
            {
                $n = count($rows) + 2;

                $rightColsBorder = [
                    'A3:A'.$n,
                    'B3:B'.$n,
                    'C3:C'.$n,
                    'D3:D'.$n,
                    'E3:E'.$n,
                    'F3:F'.$n,
                    'G1:G'.$n
                ];

                $bottomColsBorder = [
                    'A1:H1',
                    'A2:H2',
                    'A'.$n.':G'.$n
                ];
            }
        }
        else
        {
            $rows = [];

            if ($hasAddresses)
            {
                for ($i = 0; $i < count($data['addresses']); $i++)
                {
                    $rows[$i][0] = $data['addresses'][$i]['name'];

                    if ($i == 0)
                        $rows[$i][1] = 'Рекламные носители не найдены';
            
                    if ($i > 0 && count($rows[$i]) == 1)
                        $rows[$i][1] = '';
                }

                $n = count($data['addresses']) + 2;

                $leftColsBorder = ['B3:B' . $n];

                $rightColsBorder = ['H1:H' . $n];

                $bottomColsBorder = [
                    'A1:H1',
                    'A2:H2',
                    'A' . $n . ':H' . $n
                ];
            }
            else
            {
                $rows[] = 'Рекламные носители не найдены';

                $rightColsBorder = ['G1:G3'];

                $bottomColsBorder = [
                    'A1:G1',
                    'A2:G2',
                    'A3:G3'
                ];
            }
        }

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('CLUMBA');
        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getActiveSheet()->setCellValue('A1', $data['region']['title']);
        $spreadsheet->getActiveSheet()->setCellValue('A2', $data['items_info']['title']);
        $spreadsheet->getActiveSheet()->fromArray($rows, null, 'A3');

        if ($hasAddresses)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(40);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(10);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        }
        else
        {
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(40);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        }

        foreach ($leftColsBorder as $leftColBorder)
        {
            $spreadsheet->getActiveSheet()->getStyle($leftColBorder)->applyFromArray([
                'borders' => [
                    'left' => [
                        'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
                    ],
                ],
            ]);
        }

        foreach ($rightColsBorder as $rightColBorder)
        {
            $spreadsheet->getActiveSheet()->getStyle($rightColBorder)->applyFromArray([
                'borders' => [
                    'right' => [
                        'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
                    ],
                ],
            ]);
        }

        foreach ($topColsBorder as $topColBorder)
        {
            $spreadsheet->getActiveSheet()->getStyle($topColBorder)->applyFromArray([
                'borders' => [
                    'top' => [
                        'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
                    ],
                ],
            ]);
        }

        foreach ($bottomColsBorder as $bottomColBorder)
        {
            $spreadsheet->getActiveSheet()->getStyle($bottomColBorder)->applyFromArray([
                'borders' => [
                    'bottom' => [
                        'borderStyle' => SpreadsheetStyleBorder::BORDER_THIN,
                    ],
                ],
            ]);
        }

        $writer = SpreadsheetIOFactory::createWriter($spreadsheet, 'Xls');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');

        $writer->save("php://output");
        exit;
    }

    /**
     * Сохранить результат карты.
     * 
     * @return null|string
     */
    protected function saveMapResult()
    {
        $sessionData = session()->get('excel_export');
        $uid = null;

        if (!empty($sessionData))
        {
            $uid = Str::random(24);
            $commercialOffer = new CommercialOffer();

            $commercialOffer->uid = $uid;
            $commercialOffer->data = json_encode($sessionData, JSON_UNESCAPED_UNICODE);
            $commercialOffer->date = date('Y-m-d H:i:s');
            $commercialOffer->save();
        }

        return $uid;
    }

    /**
     * Расчет точек по адресам и фильтрам на карте.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response JSON
     */
    public function ajaxCalcMapPoints(Request $request)
    {
        $request->session()->forget('excel_export');

        $params = [];
        $params['search_type'] = $request->input('search_type');
        $params['region_id'] = $request->input('region_id');
        $params['filters']['building_type_ids'] = $request->has('filters.building_type_ids') ? $request->input('filters.building_type_ids') : [];
        $params['filters']['media_type_ids'] = $request->has('filters.media_type_ids') ? $request->input('filters.media_type_ids') : [];
        $params['filters']['radius'] = $request->input('filters.radius');
        $params['filters']['polygon_coords'] = json_decode($request->input('filters.polygon_coords'), true);
        $params['addresses'] = $request->input('addresses');

        $mapPoints = AdMedium::calcMapPoints($params, null);
        $mapPointsInfo = AdMedium::calcNetworkCoverage($mapPoints);

        // Запомнить для экспорта
        $region = Region::find($request->input('region_id'));
        $request->session()->put('excel_export', [
            'search_type' => $params['search_type'],
            'region' => [
                'id' => $region->id,
                'title' => $region->title
            ],
            'filters' => $params['filters'],
            'addresses' => $params['addresses'],
            'items' => $mapPoints,
            'items_info' => $mapPointsInfo
        ]);

        return response()->json([
            'items' => $mapPoints,
            'items_info' => $mapPointsInfo
        ], 200);
    }

    /**
     * Создание гиперссылки и сохранение результата карты в БД.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response JSON
     */
    public function ajaxSaveMapResult(Request $request)
    {
        $uid = $this->saveMapResult();

        return response()->json([
            'uid' => $uid,
            'link' => ($uid) ? url('/shared/map?uid=' . $uid) : null
        ], 200);
    }

    /**
     * Сброс результата карты в сессии.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response JSON
     */
    public function ajaxForgetMapResult(Request $request)
    {
        $request->session()->forget('excel_export');

        return response()->json([
            'status' => true
        ], 200);
    }

    /**
     * Удаление точки с карты и обновление данных в сессии.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response JSON
     */
    public function ajaxRemoveMapPoint(Request $request)
    {
        $id = $request->input('id');
        $uid = $request->input('uid');
        $sessionData = $request->session()->get('excel_export');

        // Удаление точки из данных сессии
        foreach ($sessionData['items'] as $k => $v)
        {
            $mapPoints = [];
            if (!empty($v['points']))
            {
                foreach ($v['points'] as $point)
                {
                    if ($point->id != $id)
                    {
                        $mapPoints[] = $point;
                    }
                }

                $sessionData['items'][$k]['points'] = $mapPoints;
            }
        }

        $mapPointsInfo = AdMedium::calcNetworkCoverage($sessionData['items']);
        $sessionData['items_info'] = $mapPointsInfo;
        $sessionData = $sessionData;

        $request->session()->put('excel_export', $sessionData);

        // Обновление данных в БД (если была сформирована ссылка)
        if (!empty($uid))
        {
            $commercialOffer = CommercialOffer::find($uid);
            $commercialOffer->data = json_encode($sessionData, JSON_UNESCAPED_UNICODE);
            $commercialOffer->save();
        }

        return response()->json([
            'items_info' => $mapPointsInfo,
            'result' => $request->session()->get('excel_export')
        ], 200);
    }

    /**
     * Отправка заявки.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response JSON
     */
    public function ajaxSendFeedback(Request $request)
    {
        $validator = Validator::make(
            $request->except('_token'),
            [
                'name' => 'required',
                'company' => 'required',
                'phone' => 'required',
                'personal_data' => 'required'
            ]
        );

        if ($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->getMessages()
            ], 422);
        }

        $uid = (!empty($request->uid)) ? $request->uid : $this->saveMapResult();

        Mail::to(env('MAIL_TO'))->send(new FeedbackMail([
            'form' => $request->except('_token'),
            'map_link' => ($uid) ? url('/shared/map?uid=' . $uid) : null
        ]));

        return response()->json([
            'uid' => $uid,
            'link' => ($uid) ? url('/shared/map?uid=' . $uid) : null
        ], 200);
    }
}
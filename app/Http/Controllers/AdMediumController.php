<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\AdMedium;
use App\Models\AdMediumImg;
use App\Models\Region;
use App\Models\BuildingType;
use App\Models\MediaType;
use App\Http\Traits\FlashMessage;

class AdMediumController extends Controller
{
    use FlashMessage;

    /**
     * Кол-во рекламных носителей в списке.
     * 
     * @var int
     */
    protected $pageLimit = 30;

    /**
     * Список рекламных носителей.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = [];
        $params['regions'] = Region::getForList();
        $params['building_types'] = BuildingType::getForList();
        $params['media_types'] = MediaType::getForList();
        $params['number'] = ($request->query('page') && $request->query('page') > 1) ? $request->query('page') * $this->pageLimit : 1;

        $filters = [];
        $filters['ext_id'] = $request->query('ext_id') ?: null;
        $filters['region_id'] = $request->query('region_id') ?: null;
        $filters['building_type_id'] = $request->query('building_type_id') ?: null;
        $filters['media_type_id'] = $request->query('media_type_id') ?: null;

        $adMediums = AdMedium::allForTable($filters, $this->pageLimit);

        return view('ad_medium/index', [
            'params' => $params,
            'filters' => $filters,
            'ad_mediums' => $adMediums->appends($request->query()),
        ]);
    }

    /**
     * Форма создания рекламного носителя.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [];
        $params['regions'] = Region::getForList();
        $params['building_types'] = BuildingType::getForList();
        $params['media_types'] = MediaType::getForList();

        return view('ad_medium/form', [
            'params' => $params,
            'ad_medium' => false,
        ]);
    }

    /**
     * Создание рекламного носителя.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response JSON
     */
    public function ajaxStore(Request $request)
    {
        $adMedium = new AdMedium();

        $adMedium->ext_id = $request->ext_id ?: null;
        $adMedium->region_id = $request->region_id;
        $adMedium->title = $request->title;
        $adMedium->building_type_id = $request->building_type_id;
        $adMedium->media_type_id = $request->media_type_id;
        $adMedium->media_type_orig_title = $request->media_type_orig_title;
        $adMedium->size = $request->size;
        $adMedium->orientation = $request->orientation;
        $adMedium->address = $request->address;
        $adMedium->map_lat = $request->map_lat ? (float)$request->map_lat : null;
        $adMedium->map_lon = $request->map_lon ? (float)$request->map_lat : null;
        $adMedium->network_coverage = ((int)$request->network_coverage > 0) ? (int)$request->network_coverage : 0;

        $adMedium->save();

        if ($request->hasFile('files'))
        {
            $files = [];

            foreach ($request->file('files') as $file)
            {
                $name = substr(md5(mt_rand()), 0, 24) . '.' . $file->getClientOriginalExtension();

                $files[] = new AdMediumImg([
                    'name' => $name,
                    'orig_name' => $file->getClientOriginalName(),
                    'mime_type' => $file->getMimeType(),
                    'size' => $file->getSize(),
                    'extension' => $file->getClientOriginalExtension()
                ]);

                $file->move(public_path('/uploads'), $name);
            }

            $adMedium->files()->saveMany($files);
        }

        $this->flashBrand('Рекламный носитель добавлен');

        return response()->json([
            'id' => $adMedium->id,
        ], 200);
    }

    /**
     * Форма редактирования рекламного носителя.
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $params = [];
        $params['regions'] = Region::getForList();
        $params['building_types'] = BuildingType::getForList();
        $params['media_types'] = MediaType::getForList();

        $adMedium = AdMedium::find($id);

        return view('ad_medium/form', [
            'params' => $params,
            'ad_medium' => $adMedium
        ]);
    }

    /**
     * Обновление рекламного носителя.
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response JSON
     */
    public function ajaxUpdate(Request $request, $id)
    {
        $adMedium = AdMedium::find($id);

        $adMedium->ext_id = $request->ext_id ?: null;
        $adMedium->region_id = $request->region_id;
        $adMedium->title = $request->title;
        $adMedium->building_type_id = $request->building_type_id;
        $adMedium->media_type_id = $request->media_type_id;
        $adMedium->media_type_orig_title = $request->media_type_orig_title;
        $adMedium->size = $request->size;
        $adMedium->orientation = $request->orientation;
        $adMedium->address = $request->address;
        $adMedium->map_lat = (float)$request->map_lat ?: null;
        $adMedium->map_lon = (float)$request->map_lon ?: null;
        $adMedium->network_coverage = ((int)$request->network_coverage > 0) ? (int)$request->network_coverage : 0;

        $adMedium->save();

        $deleteFilesIds = json_decode($request->delete_files, true);

        if (!empty($deleteFilesIds))
        {
            foreach ($deleteFilesIds as $deleteFileId)
            {
                $file = AdMediumImg::find($deleteFileId);

                if ($file)
                {
                    unlink(public_path('/uploads/' . $file->name));
                    $file->delete();
                }
            }
        }

        if ($request->hasFile('files'))
        {
            $files = [];

            foreach ($request->file('files') as $file)
            {
                $name = substr(md5(mt_rand()), 0, 24) . '.' . $file->getClientOriginalExtension();

                $files[] = new AdMediumImg([
                    'name' => $name,
                    'orig_name' => $file->getClientOriginalName(),
                    'mime_type' => $file->getMimeType(),
                    'size' => $file->getSize(),
                    'extension' => $file->getClientOriginalExtension()
                ]);

                $file->move(public_path('/uploads'), $name);
            }

            $adMedium->files()->saveMany($files);
        }

        $this->flashBrand('Рекламный носитель обновлен');

        return response()->json([
            'id' => $adMedium->id
        ], 200);
    }

    /**
     * Удаление рекламного носителя.
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function delete(Request $request, $id)
    {
        $adMedium = AdMedium::with('files')->find($id);

        if ($adMedium->files)
        {
            foreach ($adMedium->files as $file)
            {
                unlink(public_path('/uploads/' . $file->name));
                $file->delete();
            }
        }

        $adMedium->delete();

        $this->flashBrand('Рекламный носитель удален');

        return redirect('ad-medium/list');
    }

    /**
     * Валидация рекламного носителя.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response JSON
     */
    public function ajaxValidate(Request $request)
    {
        $result = [];

        $validator = Validator::make(
            $request->all(),
            AdMedium::$rules,
            AdMedium::$rulesMessages
        );
        
        if ($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->getMessages()
            ], 422);
        }

        return response()->json([], 200);
    }
}
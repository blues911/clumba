<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdMedium;
use App\Models\BuildingType;
use App\Models\MediaType;
use App\Models\CommercialOffer;

class SharedController extends Controller
{
    /**
     * Публичная карта.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function map(Request $request)
    {
        $uid = $request->get('uid');
        $commercialOffer = CommercialOffer::where('uid', $uid)->firstOrFail();
        $commercialOffer = json_decode($commercialOffer->data, true);

        $sharedIds = [];
        foreach ($commercialOffer['items'] as $k => $v)
        {
            if (!empty($v['points']))
            {
                foreach ($v['points'] as $point)
                {
                    $sharedIds[] = $point['id'];
                }
            }
        }

        $adMediumIds = AdMedium::calcMapPointsIds([
            'search_type' => $commercialOffer['search_type'],
            'region_id' => $commercialOffer['region']['id'],
            'filters' => [
                'building_type_ids' => $request->has('building_type_ids') ? $request->input('building_type_ids') : [],
                'media_type_ids' => $request->has('media_type_ids') ? $request->input('media_type_ids') : [],
                'radius' => $commercialOffer['filters']['radius'],
                'polygon_coords' => $commercialOffer['filters']['polygon_coords'],
            ],
            'addresses' => $commercialOffer['addresses']
        ], $sharedIds);

        $params = [];
        $params['search_type'] = $commercialOffer['search_type'];
        $params['building_types'] = BuildingType::getForMap($adMediumIds);
        $params['media_types'] = MediaType::getForMap($adMediumIds);
        $params['uid'] = $uid;

        return view('map', [
            'params' => $params,
            'filters' => $commercialOffer['filters'],
        ]);
    }

    /**
     * Расчет точек по адресам и фильтрам на карте.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response JSON
     */
    public function ajaxCalcMapResult(Request $request)
    {
        $uid = $request->input('uid');
        $result = [];

        $commercialOffer = CommercialOffer::where('uid', $uid)->first();

        if ($commercialOffer)
        {
            $commercialOffer = json_decode($commercialOffer->data, true);

            $sharedIds = [];
            foreach ($commercialOffer['items'] as $k => $v)
            {
                if (!empty($v['points']))
                {
                    foreach ($v['points'] as $point)
                    {
                        $sharedIds[] = $point['id'];
                    }
                }
            }

            $result = AdMedium::calcMapPoints([
                'search_type' => $commercialOffer['search_type'],
                'region_id' => $commercialOffer['region']['id'],
                'filters' => [
                    'building_type_ids' => $request->has('building_type_ids') ? $request->input('building_type_ids') : [],
                    'media_type_ids' => $request->has('media_type_ids') ? $request->input('media_type_ids') : [],
                    'radius' => $commercialOffer['filters']['radius'],
                    'polygon_coords' => $commercialOffer['filters']['polygon_coords'],
                ],
                'addresses' => $commercialOffer['addresses']
            ], $sharedIds);

            foreach ($result as $k => $v)
            {
                $result[$k]['radius'] = $commercialOffer['filters']['radius'];
            }
        }

        return response()->json($result, 200);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Glide\ServerFactory as Glide;

class GlideController extends Controller
{
    public function handle(Request $request, $path)
    {
        $glide = Glide::create([
            'source' => public_path('uploads/'),
            'cache' => storage_path('framework/cache/glide/')
        ]);

        $glide->outputImage($path, $request->query());
    }
}
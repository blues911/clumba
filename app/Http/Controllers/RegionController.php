<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Region;
use App\Http\Traits\FlashMessage;

class RegionController extends Controller
{
    use FlashMessage;

    /**
     * Кол-во регионов в списке.
     * 
     * @var int
     */
    protected $pageLimit = 30;

    /**
     * Список регионов.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params['number'] = ($request->query('page') && $request->query('page') > 1) ? $request->query('page') * $this->pageLimit : 1;

        $regions = Region::paginate($this->pageLimit);

        return view('region/index', [
            'params' => $params,
            'regions' => $regions
        ]);
    }

    /**
     * Форма создания региона.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('region/form', [
            'region' => false
        ]);
    }

    /**
     * Создание региона.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            Region::$rules,
            Region::$rulesMessages
        )->validate();

        $region = new Region;
        $region->title = $request->input('title');
        $region->save();

        $this->flashBrand('Регион добавлен');

        return redirect('dictionary/region/list');
    }

    /**
     * Форма редактирования региона.
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $region = Region::findOrFail($id);

        return view('region/form', [
            'region' => $region
        ]);
    }

    /**
     * Обновление региона.
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        Validator::make(
            $request->all(),
            Region::$rules,
            Region::$rulesMessages
        )->validate();

        $region = Region::findOrFail($id);
        $region->title = $request->input('title');
        $region->save();

        $this->flashBrand('Регион обновлен');

        return redirect('dictionary/region/list');
    }

    /**
     * Удаление региона.
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function delete(Request $request, $id)
    {
        $region = Region::findOrFail($id);
        $region->delete();

        $this->flashBrand('Регион удален');

        return redirect('dictionary/region/list');
    }
}
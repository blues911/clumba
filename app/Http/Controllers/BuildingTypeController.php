<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\BuildingType;
use App\Http\Traits\FlashMessage;

class BuildingTypeController extends Controller
{
    use FlashMessage;

    /**
     * Кол-во типов локации в списке.
     * 
     * @var int
     */
    protected $pageLimit = 30;

    /**
     * Список типов локации.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params['number'] = ($request->query('page') && $request->query('page') > 1) ? $request->query('page') * $this->pageLimit : 1;

        $buildingTypes = BuildingType::paginate($this->pageLimit);

        return view('building_type/index', [
            'params' => $params,
            'building_types' => $buildingTypes
        ]);
    }

    /**
     * Форма создания типа локации.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('building_type/form', [
            'building_type' => false
        ]);
    }

    /**
     * Создание типа локации.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            BuildingType::$rules,
            BuildingType::$rulesMessages
        )->validate();

        $buildingType = new BuildingType;
        $buildingType->title = $request->input('title');
        $buildingType->save();

        $this->flashBrand('Тип локации добавлен');

        return redirect('dictionary/building-type/list');
    }

    /**
     * Форма редактирования типа локации.
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buildingType = BuildingType::findOrFail($id);

        return view('building_type/form', [
            'building_type' => $buildingType
        ]);
    }

    /**
     * Обновление типа локации.
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        Validator::make(
            $request->all(),
            BuildingType::$rules,
            BuildingType::$rulesMessages
        )->validate();

        $buildingType = BuildingType::findOrFail($id);
        $buildingType->title = $request->input('title');
        $buildingType->save();

        $this->flashBrand('Тип локации обновлен');

        return redirect('dictionary/building-type/list');
    }

    /**
     * Удаление рекламного носителя.
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function delete(Request $request, $id)
    {
        $buildingType = BuildingType::findOrFail($id);
        $buildingType->delete();

        $this->flashBrand('Тип локации удален');

        return redirect('dictionary/building-type/list');
    }
}
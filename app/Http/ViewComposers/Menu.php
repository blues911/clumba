<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Chat;

class Menu
{
    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(){}

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        // Меню в Личном Кабинете
        $menu = [];

        if (auth()->check())
        {
            if (auth()->user()->type == 'admin')
            {
                $menu[] = [
                    'title' => 'Коммерческие предложения',
                    'path' => '/commercial-offer/list'
                ];
    
                $menu[] = [
                    'title' => 'Рекламные носители',
                    'path' => '/ad-medium/list'
                ];
    
                $menu[] = [
                    'title' => 'Справочники',
                    'path' => '/dictionary',
                    'sub_menu' => [
                        [
                            'title' => 'Регионы',
                            'path' => '/dictionary/region/list'
                        ],
                        [
                            'title' => 'Типы локации',
                            'path' => '/dictionary/building-type/list'
                        ],
                        [
                            'title' => 'Типы носителя',
                            'path' => '/dictionary/media-type/list'
                        ]
                    ]
                ];
            }
            else
            {
                $menu[] = [
                    'title' => 'Коммерческие предложения',
                    'path' => '/commercial-offer/list'
                ];
            }
        }

        $view->with('menu', $menu);
    }
}
<?php

if (!function_exists('highlight_menu'))
{
    function highlight_menu($path)
    {
        $parseUrl = parse_url(url()->current());
        $uri = explode('/', $parseUrl['path']);
        $path = explode('/', $path);

        if ($uri[1] == $path[1])
        {
            return true;
        }

        return false;
    }
}

if (!function_exists('highlight_submenu'))
{
    function highlight_submenu($path)
    {
        $parseUrl = parse_url(url()->current());
        $uri = explode('/', $parseUrl['path']);
        $path = explode('/', $path);

        if (count($uri) > 2 && $uri[2] == $path[2])
        {
            return true;
        }

        return false;
    }
}

if (!function_exists('calc_map_points_distance'))
{
    function calc_map_points_distance($φA, $λA, $φB, $λB)
    {
        $EARTH_RADIUS = 6372795;

        // перевести координаты в радианы
        $lat1 = $φA * M_PI / 180;
        $lat2 = $φB * M_PI / 180;
        $lon1 = $λA * M_PI / 180;
        $lon2 = $λB * M_PI / 180;

        // косинусы и синусы широт и разницы долгот
        $cl1 = cos($lat1);
        $cl2 = cos($lat2);
        $sl1 = sin($lat1);
        $sl2 = sin($lat2);
        $delta = $lon2 - $lon1;
        $cdelta = cos($delta);
        $sdelta = sin($delta);

        // вычисления длины большого круга
        $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
        $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;
        
        //
        $ad = atan2($y, $x);
        $dist = $ad * $EARTH_RADIUS;
        
        return $dist;
    }
}

if (!function_exists('calc_map_point_in_polygon'))
{
    function calc_map_point_in_polygon($pointCoords, $polygonCoords)
    {
        // Если оперируете (сотнями) тысячами точек
        // set_time_limit(60);

        $p = $pointCoords;
        $polygon = $polygonCoords;

        $c = 0;
        $p1 = $polygon[0];
        $n = count($polygon);

        for ($i = 1; $i <= $n; $i++)
        {
            $p2 = $polygon[$i % $n];

            if ($p[1] > min($p1[1], $p2[1])
                && $p[1] <= max($p1[1], $p2[1])
                && $p[0] <= max($p1[0], $p2[0])
                && $p1[1] != $p2[1])
                {
                    $xinters = ($p[1] - $p1[1]) * ($p2[0] - $p1[0]) / ($p2[1] - $p1[1]) + $p1[0];

                    if ($p1[0] == $p2[0] || $p[0] <= $xinters)
                    {
                        $c++;
                    }
            }

            $p1 = $p2;
        }

        // Если количество ребер, через которые мы прошли - четное,
        // тогда точка не в полигоне
        return $c % 2 != 0;
    }
}

if (!function_exists('snumb_format'))
{
    function snumb_format($num)
    {
        // 15,000,000.0100 -> 15 000 000.01
        // 15,000,000.0000 -> 15 000 000

        $num = number_format($num, 2);

        if (preg_match('/(.).00/', $num))
            $num = str_replace('.00', '', $num);

        $num = str_replace(',', ' ', $num);

        return $num;
    }
}
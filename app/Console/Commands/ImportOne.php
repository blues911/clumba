<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\Region;
use App\Models\BuildingType;
use App\Models\MediaType;
use App\Models\AdMedium;
use App\Models\AdMediumImg;

class ImportOne extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:excel_one';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import (one-time) advertising medium data from excel file into database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = scandir(public_path('/import'));

        if (!empty($files) && isset($files[2]) && preg_match('/\.(xls|xlsx)$/', $files[2]))
        {
            $file = public_path('/import/'.$files[2]);
            $fileType = IOFactory::identify($file);
            $reader = IOFactory::createReader($fileType);
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($file);

            $items = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $result = [];

            $tmpRows = [];
            $tmpRows['regions'] = [];
            $tmpRows['building_types'] = [];
            $tmpRows['media_types'] = [
                'Led-панель / монитор' => [
                    'Led-панель / монитор',
                    'Видеостена',
                    'Видеоэкран',
                    'Диджитал менюборд',
                    'Медиафасад',
                    'Платежный терминал',
                    'Тейбл Тент',
                    'Терминал заказа и оплаты',
                    'Тилтоппер',
                    'Экран в к/т',
                ],
                'Лайтбокс' => [
                    'Лайтбокс',
                ],
                'Постер' => [
                    'Постер'
                ],
                'Напольная стойка' => [
                    'Напольная стойка'
                ]
            ];

            foreach ($items as $k => $item)
            {
                if ($k > 2 && $item['C'] != '')
                {
                    $tmpRows['regions'][] = $item['E'];
                    $tmpRows['building_types'][] = $item['B'];
                }
            }

            $tmpRows['regions'] = array_unique($tmpRows['regions']);
            $tmpRows['building_types'] = array_unique($tmpRows['building_types']);

            foreach ($tmpRows['regions'] as $item)
            {
                $region = new Region;
                $region->title = $item;
                $region->save();
            }

            $buildingType = new BuildingType;
            $buildingType->title = 'Бургер Кинг';
            $buildingType->save();

            foreach ($tmpRows['building_types'] as $item)
            {
                if ($item != 'Торговый центр Баинг')
                {
                    $buildingType = new BuildingType;
                    $buildingType->title = $item;
                    $buildingType->save();
                }
            }

            foreach ($tmpRows['media_types'] as $name => $aliases)
            {
                $mediaType = new MediaType;
                $mediaType->title = $name;
                $mediaType->aliases = implode("\n", $aliases);
                $mediaType->save();
            }

            echo '[import] start' . PHP_EOL;

            foreach ($items as $k => $item)
            {
                if ($k > 2 && $item['H'] != '')
                {
                    $region = Region::where('title', $item['E'])->first();
                    $buildingType = BuildingType::where('title', ($item['B'] == 'Торговый центр Баинг') ? 'Торговый центр' : $item['B'])->first();
                    $mediaType = MediaType::where('aliases', 'like', '%' . $item['C'] . '%')->first();

                    $hash = $item['A'].$item['E'].$item['B'].$item['C'].$item['H'].$item['I'].$item['W'].$item['F'].$item['L'];
                    $hash = md5($hash);

                    $result[] = [
                        'ext_id' => $item['A'],
                        'region_id' => $region ? $region->id : null,
                        'building_type_id' => $buildingType ? $buildingType->id : null,
                        'media_type_id' => $mediaType ? $mediaType->id : null,
                        'media_type_orig_title' => $item['C'],
                        'title' => $item['H'],
                        'address' => $item['F'],
                        'size' => $item['I'],
                        'orientation' => ($item['W'] != '' && in_array($item['W'], ['вертикальный', 'горизонтальный'])) ? $item['W'] : 'вертикальный',
                        'images' => explode(';', $item['L']),
                        'hash' => $hash
                    ];
                }
            }

            if (!empty($result))
            {
                foreach ($result as $item)
                {
                    $oldAdMedium = AdMedium::where('ext_id', $item['ext_id'])->first();

                    if (!$oldAdMedium)
                    {
                        $adMedium = new AdMedium();

                        $adMedium->ext_id = $item['ext_id'];
                        $adMedium->region_id = $item['region_id'];
                        $adMedium->building_type_id = $item['building_type_id'];
                        $adMedium->media_type_id = $item['media_type_id'];
                        $adMedium->media_type_orig_title = $item['media_type_orig_title'];
                        $adMedium->title = $item['title'];
                        $adMedium->address = $item['address'];
                        $adMedium->size = $item['size'];
                        $adMedium->orientation = $item['orientation'];
                        $adMedium->hash = $item['hash'];
                        $adMedium->save();
    
                        if (!empty($item['images']))
                        {
                            $files = [];
    
                            foreach ($item['images'] as $imageUrl)
                            {
                                $fileName = basename($imageUrl);
    
                                if (preg_match('/^.*\.(jpg|jpeg|png|tiff|bmp)$/i', $fileName))
                                {
                                    if ($file_contents = @file_get_contents($imageUrl))
                                    {
                                        file_put_contents(public_path('/uploads/' . $fileName), $file_contents);
                                        $ext = pathinfo(public_path('/uploads/' . $fileName), PATHINFO_EXTENSION);
                                        $newName = substr(md5(mt_rand()), 0, 24) . '.' . $ext;
            
                                        $files[] = new AdMediumImg([
                                            'name' => $newName,
                                            'orig_name' => $fileName,
                                            'mime_type' => mime_content_type(public_path('/uploads/' . $fileName)),
                                            'size' => filesize(public_path('/uploads/' . $fileName)),
                                            'extension' => $ext
                                        ]);
            
                                        rename(public_path('/uploads/' . $fileName), public_path('/uploads/' . $newName));
                                    }
                                }
                            }
    
                            if (count($files) > 0)
                            {
                                $adMedium->files()->saveMany($files);
                            }
                        }
                    }
                }

                echo '[import] total: ' . count($result) . PHP_EOL;
                echo '[import] finish' . PHP_EOL;

                echo '[map coords] start' . PHP_EOL;

                $items = AdMedium::with('region')
                    ->where('address', '!=', '')
                    ->where('map_lat', '=', null)
                    ->where('map_lon', '=', null)
                    ->get();

                $total = 0;
                if ($items)
                {
                    foreach ($items as $item)
                    {
                        $params = [
                            'geocode' => $item->region->title . ', ' . $item->address,
                            'format' => 'json',
                            'results' => 1,
                            'apikey' => env('YANDEX_MAP_API_KEY')
                        ];

                        $res = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
        
                        if ($res && !empty($res->response->GeoObjectCollection->featureMember))
                        {
                            $coords = explode(' ', $res->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
        
                            $item->map_lon = $coords[0];
                            $item->map_lat = $coords[1];
                            $item->save();
        
                            $total++;
                        }
                    }
                }

                echo '[map coords] total: ' . $total . PHP_EOL;
                echo '[map coords] finish' . PHP_EOL;
            }
        }
        else
        {
            echo "No excel file." . PHP_EOL;
        }
    }
}
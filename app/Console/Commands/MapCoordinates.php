<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\AdMedium;

class MapCoordinates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map:init_coords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize map coordinates for ad_medium.region + ad_medium.address.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $items = AdMedium::with('region')
            ->where('address', '!=', '')
            ->where('map_lat', '=', null)
            ->where('map_lon', '=', null)
            ->get();

        echo '[map coords] start' . PHP_EOL;

        $total = 0;
        if ($items)
        {
            foreach ($items as $item)
            {
                $params = [
                    'geocode' => $item->region->title . ', ' . $item->address,
                    'format' => 'json',
                    'results' => 1,
                    'apikey' => env('YANDEX_MAP_API_KEY')
                ];

                $res = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));

                if ($res && !empty($res->response->GeoObjectCollection->featureMember))
                {
                    $coords = explode(' ', $res->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);

                    $item->map_lon = $coords[0];
                    $item->map_lat = $coords[1];
                    $item->save();

                    $total++;
                }
            }
        }

        echo '[map coords] total: ' . $total . PHP_EOL;
        echo '[map coords] finish' . PHP_EOL;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserUpdatePassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:update_password {user_id : The ID of the user} {new_password : The new password for the user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update user password.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();

        if (isset($arguments['user_id']) && isset($arguments['new_password']))
        {
            $user = User::find($arguments['user_id']);

            if ($user)
            {
                $user->password = Hash::make($arguments['new_password']);
                $user->save();

                echo 'User password updated' . PHP_EOL;
            }
            else
            {
                echo 'User not found' . PHP_EOL;
            }

        }
    }
}

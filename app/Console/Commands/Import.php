<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\Region;
use App\Models\BuildingType;
use App\Models\MediaType;
use App\Models\AdMedium;
use App\Models\AdMediumImg;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import advertising medium data from json file into database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileName = null;
        $hashList = [];
        $matchedFiles = [];
        $keys = [
            'ext_id',
            'region',
            'building_type',
            'media_type',
            'title',
            'size',
            'orientation',
            'address',
            'photos',
            'network_coverage'
        ];

        $dir = array_slice(scandir(public_path('/import')), 2);

        // Проверка .json.lock (который уже в обработке)
        $matchedFiles = preg_grep('/(.*).json.lock$/i', $dir);
        if (count($matchedFiles) > 0) die;

        $matchedFiles = preg_grep('/(.*).json$/i', $dir);
        if (count($matchedFiles) == 0) die;

        $fileName = array_shift($matchedFiles);
        rename(public_path('/import/'.$fileName), public_path('/import/'.$fileName.'.lock'));
        $fileName .= '.lock';

        $rows = file_get_contents(public_path('/import/'.$fileName), false);
        $rows = json_decode($rows, true);

        if (empty($rows)) die;

        foreach ($rows as $row)
        {
            if (empty($row)) continue;

            $strKeys = array_keys($row);
            if (count(array_diff($keys, $strKeys)) != 0) continue;

            $hash = '';
            foreach ($row as $k => $v)
            {
                $hash .= $v;
            }

            $hash = md5($hash);
            $hashList[] = $hash;

            // Проверка дубля данных согласно hash
            $adMediumDouble = AdMedium::where('hash', $hash)->first();
            if ($adMediumDouble) continue;

            // Проверка "региона"
            $region = Region::where('title', $row['region'])->first();
            if (!$region)
            {
                $region = new Region;
                $region->title = $row['region'];
                $region->save();
            }

            // Проверка "типа локации"
            if (mb_strtolower($row['building_type']) == 'торговый центр баинг'
                || mb_strtolower($row['building_type']) == 'тц баинг')
            {
                $row['building_type'] = 'Торговый центр';
            }
            $buildingType = BuildingType::where('title', $row['building_type'])->first();
            if (!$buildingType)
            {
                $buildingType = new BuildingType;
                $buildingType->title = $row['building_type'];
                $buildingType->save();
            }

            // Проверка "типа носителя"
            $mediaType = MediaType::where('title', $row['media_type'])
                ->orWhere('aliases', 'like', '%' . $row['media_type'] . '%')
                ->first();
            if (!$mediaType)
            {
                $mediaType = new MediaType;
                $mediaType->title = $row['media_type'];
                $mediaType->save();
            }

            // Проверка "1С кода"
            $adMediumOld = AdMedium::with('files')->where('ext_id', $row['ext_id'])->first();
            if ($adMediumOld)
            {
                if ($adMediumOld->files)
                {
                    foreach ($adMediumOld->files as $f)
                    {
                        if (file_exists(public_path('/uploads/'.$f->name)))
                        {
                            unlink(public_path('/uploads/'.$f->name));
                        }

                        $f->delete();
                    }
                }

                $adMediumOld->delete();
            }

            $adMedium = new AdMedium();

            $adMedium->ext_id = $row['ext_id'];
            $adMedium->region_id = $region->id;
            $adMedium->building_type_id = $buildingType->id;
            $adMedium->media_type_id = $mediaType->id;
            $adMedium->media_type_orig_title = $row['media_type'];
            $adMedium->title = $row['title'];
            $adMedium->address = $row['address'];
            $adMedium->size = $row['size'];
            $adMedium->orientation = (in_array(strtolower($row['orientation']), ['вертикальный', 'горизонтальный'])) ? strtolower($row['orientation']) : 'вертикальный';
            $adMedium->network_coverage = ((int)$row['network_coverage'] > 0) ? (int)$row['network_coverage'] : 0;
            $adMedium->hash = $hash;

            $adMedium->save();

            // Сохранение изображений
            $imageUrls = [];
            $imageUrls = explode(';', $row['photos']);
            if (!empty($imageUrls))
            {
                $images = [];

                foreach ($imageUrls as $imageUrl)
                {
                    $imageUrl = trim($imageUrl);
                    $origName = basename($imageUrl);
      
                    if (preg_match('/^.*\.(jpg|jpeg|png|tiff|bmp)$/i', $origName))
                    {
                        if ($file_contents = @file_get_contents($imageUrl))
                        {
                            file_put_contents(public_path('/uploads/' . $origName), $file_contents);
                            $ext = pathinfo(public_path('/uploads/' . $origName), PATHINFO_EXTENSION);
                            $newName = substr(md5(mt_rand()), 0, 24) . '.' . $ext;

                            $images[] = new AdMediumImg([
                                'name' => $newName,
                                'orig_name' => $origName,
                                'mime_type' => mime_content_type(public_path('/uploads/' . $origName)),
                                'size' => filesize(public_path('/uploads/' . $origName)),
                                'extension' => $ext
                            ]);

                            rename(public_path('/uploads/' . $origName), public_path('/uploads/' . $newName));
                        }
                    }
                }

                if (!empty($images))
                {
                    $adMedium->files()->saveMany($images);
                }
            }

            // Определение координат
            $adMedium = AdMedium::with('region')->find($adMedium->id);
            if ($adMedium && $adMedium->region)
            {
                $geoParams = [
                    'geocode' => $adMedium->region->title . ', ' . $adMedium->address,
                    'format' => 'json',
                    'results' => 1,
                    'apikey' => env('YANDEX_MAP_API_KEY')
                ];

                $res = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($geoParams, '', '&')));

                if ($res && !empty($res->response->GeoObjectCollection->featureMember))
                {
                    $coords = explode(' ', $res->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);

                    $adMedium->map_lon = $coords[0];
                    $adMedium->map_lat = $coords[1];
                    $adMedium->save();
                }
            }
        }

        // Удаление записей из таблицы которых нет в файле
        if (!empty($hashList))
        {
            $adMediums = AdMedium::with('files')->whereNotIn('hash', $hashList)->get();

            if ($adMediums)
            {
                foreach ($adMediums as $adMedium)
                {
                    if ($adMedium->files)
                    {
                        foreach ($adMedium->files as $f)
                        {
                            if (file_exists(public_path('/uploads/'.$f->name)))
                            {
                                unlink(public_path('/uploads/'.$f->name));
                            }
    
                            $f->delete();
                        }
                    }
    
                    $adMedium->delete();
                }
            }
        }

        // Удаление файла импорта
        unlink(public_path('/import/'.$fileName));

        die;
    }
}
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Правила валидации
        Validator::extend('map_coordinates', function ($attribute, $value, $parameters, $validator)
        {
            $data = $validator->getData();

            return ($data['map_lon'] == '' || $data['map_lat'] == '') ? false : true;
        });

        // Сообщения для правил валидации
        Validator::replacer('map_coordinates', function ($message, $attribute, $rule, $parameters)
        {
            return 'Не выбрана метка на карте.';
        });

        // Остальные сообщения в моделях...
    }
}
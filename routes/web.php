<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('', 'commercial-offer/list');
Route::get('images/{path}', 'GlideController@handle');

Route::group(['prefix' => 'shared'], function() {
    // Публичная карта
    Route::get('map', 'SharedController@map');
    // ajax
    Route::post('ajax-calc-map-result', 'SharedController@ajaxCalcMapResult');

    // !Yandex запрещает импользование карт в закрытых разделах. По этому методы
    // ниже перешли из закрытого раздела в публичную часть.

    // Коммерческие предложения (публичная часть)
    Route::get('commercial-offer', 'CommercialOfferController@index');

    // Методы используются в 2х разделах:
    // /commercial-offer/list - для авторизованных пользователей
    // /shared/commercial-offer - для гостей
    Route::get('excel-export', 'CommercialOfferController@excelExport');
    // ajax
    Route::post('ajax-calc-map-points', 'CommercialOfferController@ajaxCalcMapPoints');
    Route::post('ajax-save-map-result', 'CommercialOfferController@ajaxSaveMapResult');
    Route::post('ajax-forget-map-result', 'CommercialOfferController@ajaxForgetMapResult');
    Route::post('ajax-remove-map-point', 'CommercialOfferController@ajaxRemoveMapPoint');
    Route::post('ajax-send-feedback', 'CommercialOfferController@ajaxSendFeedback');
});

// Авторизация
Route::get('login', 'AuthController@showLoginForm')->name('login');
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');

Route::group(['middleware' => 'auth'], function() {

    // Рекламные носители
    Route::group(['prefix' => 'ad-medium', 'middleware' => 'admin'], function() {
        Route::redirect('', 'ad-medium/list');
        Route::get('list', 'AdMediumController@index');
        Route::get('create', 'AdMediumController@create');
        Route::get('edit/{id}', 'AdMediumController@edit');
        Route::post('delete/{id}', 'AdMediumController@delete');
        // ajax
        Route::post('ajax-validate', 'AdMediumController@ajaxValidate');
        Route::post('ajax-store', 'AdMediumController@ajaxStore');
        Route::post('ajax-update/{id}', 'AdMediumController@ajaxUpdate');
    });

    // Коммерческие предложения
    Route::group(['prefix' => 'commercial-offer'], function() {
        Route::redirect('', 'commercial-offer/list');
        Route::get('list', 'CommercialOfferController@index');
    });

    // Справочники
    Route::group(['prefix' => 'dictionary', 'middleware' => 'admin'], function() {

        Route::redirect('', 'region/list');

        Route::get('region/list', 'RegionController@index');
        Route::get('region/create', 'RegionController@create');
        Route::post('region/store', 'RegionController@store');
        Route::get('region/edit/{id}', 'RegionController@edit');
        Route::post('region/update/{id}', 'RegionController@update');
        Route::post('region/delete/{id}', 'RegionController@delete');

        Route::get('building-type/list', 'BuildingTypeController@index');
        Route::get('building-type/create', 'BuildingTypeController@create');
        Route::post('building-type/store', 'BuildingTypeController@store');
        Route::get('building-type/edit/{id}', 'BuildingTypeController@edit');
        Route::post('building-type/update/{id}', 'BuildingTypeController@update');
        Route::post('building-type/delete/{id}', 'BuildingTypeController@delete');

        Route::get('media-type/list', 'MediaTypeController@index');
        Route::get('media-type/create', 'MediaTypeController@create');
        Route::post('media-type/store', 'MediaTypeController@store');
        Route::get('media-type/edit/{id}', 'MediaTypeController@edit');
        Route::post('media-type/update/{id}', 'MediaTypeController@update');
        Route::post('media-type/delete/{id}', 'MediaTypeController@delete');
    });
});